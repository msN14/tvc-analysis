"""
*******************************************************************************
                        READING POINT DATA FROM SRP FILE (B1 STEADY)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Read Point Data from Fluent .srp file
...............................................................................
REFERENCES:

...............................................................................
DEVELOPER INFORMATION: NISANTH M. S,
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------

#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# Species Flow Rate (mdot)                                  [g/s]            
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def readSRP(fileLocation):

    with open(fileLocation, "r") as f:
        lines = f.readlines()

    O2 = {} # [g/s]
    for i in [5,6,7]:
        l = lines[i].strip().split(" ", 1)
        O2[l[0]] = float(l[1].strip())
        
    N2 = {} # [g/s]
    for i in [14,15,16]:
        l = lines[i].strip().split(" ", 1)
        N2[l[0]] = float(l[1].strip())

    CO = {} # [g/s]
    for i in [23,24,25]:
        l = lines[i].strip().split(" ", 1)
        CO[l[0]] = float(l[1].strip())
    
    NO = {} # [g/s]
    for i in [32,33,34]:
        l = lines[i].strip().split(" ", 1)
        NO[l[0]] = float(l[1].strip())

    NO2 = {} # [g/s]
    for i in [41,42,43]:
        l = lines[i].strip().split(" ", 1)
        NO2[l[0]] = float(l[1].strip())

    CH4 = {} # [g/s]
    for i in [50,51,52]:
        l = lines[i].strip().split(" ", 1)
        CH4[l[0]] = float(l[1].strip())

    CH = {} # [g/s]
    for i in [59,60,61]:
        l = lines[i].strip().split(" ", 1)
        CH[l[0]] = float(l[1].strip())
    
    CH2 = {} # [g/s]
    for i in [68,69,70]:
        l = lines[i].strip().split(" ", 1)
        CH2[l[0]] = float(l[1].strip())
 
    CH3 = {} # [g/s]
    for i in [77,78,79]:
        l = lines[i].strip().split(" ", 1)
        CH3[l[0]] = float(l[1].strip())

    C2H = {} # [g/s]
    for i in [86,87,88]:
        l = lines[i].strip().split(" ", 1)
        C2H[l[0]] = float(l[1].strip())    
        
    C2H2 = {} # [g/s]
    for i in [95,96,97]:
        l = lines[i].strip().split(" ", 1)
        C2H2[l[0]] = float(l[1].strip()) 
        
    C2H3 = {} # [g/s]
    for i in [104,105,106]:
        l = lines[i].strip().split(" ", 1)
        C2H3[l[0]] = float(l[1].strip()) 
        
    C2H4 = {} # [g/s]
    for i in [113,114,115]:
        l = lines[i].strip().split(" ", 1)
        C2H4[l[0]] = float(l[1].strip()) 
        
    C2H5 = {} # [g/s]
    for i in [122,123,124]:
        l = lines[i].strip().split(" ", 1)
        C2H5[l[0]] = float(l[1].strip()) 

    C2H6 = {} # [g/s]
    for i in [131,132,133]:
        l = lines[i].strip().split(" ", 1)
        C2H6[l[0]] = float(l[1].strip()) 
        
    C3H7 = {} # [g/s]
    for i in [140,141,142]:
        l = lines[i].strip().split(" ", 1)
        C3H7[l[0]] = float(l[1].strip()) 
        
    C3H8 = {} # [g/s]
    for i in [149,150,151]:
        l = lines[i].strip().split(" ", 1)
        C3H8[l[0]] = float(l[1].strip()) 

    return {"O2":O2, "N2":N2, "CO":CO, "NO":NO, "NO2":NO2, "CH4":CH4,
            "CH":CH, "CH2":CH2, "CH3":CH3, "C2H":C2H, "C2H2":C2H2, "C2H3":C2H3,
            "C2H4":C2H4, "C2H5":C2H5, "C2H6":C2H6, "C3H7":C3H7, "C3H8":C3H8}
#------------------------------------------------------------------------------

#---------------------------------------------[Usage]--------------------------
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
#------------------------------------------------------------------------------