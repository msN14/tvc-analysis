"""
*******************************************************************************
                    EMISSION INDEX FROM SIMULATION DATA (B1 STEADY)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Read Point Data from Fluent .srp file
    2. The .srp file data is used for EI calculations
...............................................................................
REFERENCES:

...............................................................................
DEVELOPER INFORMATION: NISANTH M. S,
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------

#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# Species Flow Rate (mdot)                                  [g/s]            
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def EI_Bi(data):
    mdot_fuel = (data["CH4"]["cavity_inlet"] + data["CH4"]["main_inlet"]) # [g/s]
    
    EICO = (abs(data["CO"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    
    EICH4 = (abs(data["CH4"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    
    # Need to check direct addition
    mdot_CxHy = data["CH4"]["outlet"] + data["CH"]["outlet"] + data["CH2"]["outlet"]+\
                data["CH3"]["outlet"] + data["C2H"]["outlet"] + data["C2H2"]["outlet"]+\
                data["C2H3"]["outlet"]+ data["C2H4"]["outlet"]+ data["C2H5"]["outlet"]+\
                data["C2H6"]["outlet"]+data["C3H7"]["outlet"]+data["C3H8"]["outlet"]
                
    EICxHy = (abs(mdot_CxHy)/mdot_fuel)*1000 # [g/Kg Fuel]
    
    EINO = (abs(data["NO"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    EINO2 = (abs(data["NO2"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    mdot_NOx = abs(data["NO"]["outlet"]) + abs(data["NO2"]["outlet"])
    EINOx = (mdot_NOx/mdot_fuel)*1000 # [g/Kg Fuel]
    
    Hc = 50e6 # Lower heating value of the fuel (LHV)  
    
    # Equation for combustion efficiency calculation
    eff=(1-(10109*(EICO/Hc))-(EICH4/1000))*100

    return {"EICO":round(EICO,2), "EICH4":round(EICH4,2), 
            "EICxHy":round(EICxHy,2), "EINO":round(EINO,2), 
            "EINO2":round(EINO2,2), "EINOx":round(EINOx,2), "Eff":round(eff,2)}
#------------------------------------------------------------------------------



#---------------------------------------------[readSRP]------------------------
def O2Basis(data):

    O2 = data["O2"]["outlet"]*100 # [%]
    CO = data["CO"]["outlet"]*1e6 # [ppm]
    NO = data["NO"]["outlet"]*1e6 # [ppm]
    NO2 = data["NO2"]["outlet"]*1e6 # [ppm]
    NOx = NO + NO2
    CO2 = data["CO2"]["outlet"]*100 # [%]
    CH4 = data["CH4"]["outlet"]*1e6 # [ppm]

    return {"O2":round(O2,2), "CO":round(CO,2), "NO":round(NO,2),
            "NO2":round(NO2,2), "NOx":round(NOx,2), "CO2":round(CO2,2),
            "CH4":round(CH4,2)}
#------------------------------------------------------------------------------

#---------------------------------------------[Usage]--------------------------
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
#------------------------------------------------------------------------------