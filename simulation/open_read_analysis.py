"""
*******************************************************************************
            EMISSION IN Xi FROM SIMULATION DATA (OPEN COMBUSTOR STEADY)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Read Point Data from Fluent .srp/.data file
    2. The .srp/.data file data is used for emission analysis
    
    Include functions from developement .py files, listed below :
        
        analysis_Bi.py + read_mdot_species.py + read_x_species.py
...............................................................................
REFERENCES:

...............................................................................
DEVELOPER INFORMATION: NISANTH M. S,
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------
# import emissionO2basis as eo2b
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# Species Flow Rate (mdot)                                  [g/s]            
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def read_X(fileLocation):

    with open(fileLocation, "r") as f:
        lines = f.readlines()

    O2 = {} # [-]
    for i in [5,6,7]:
        l = lines[i].strip().split(" ", 1)
        O2[l[0]] = float(l[1].strip())
        
    N2 = {} # [-]
    for i in [14,15,16]:
        l = lines[i].strip().split(" ", 1)
        N2[l[0]] = float(l[1].strip())

    CO = {} # [-]
    for i in [23,24,25]:
        l = lines[i].strip().split(" ", 1)
        CO[l[0]] = float(l[1].strip())
    
    CO2 = {} # [-]
    for i in [32,33,34]:
        l = lines[i].strip().split(" ", 1)
        CO2[l[0]] = float(l[1].strip())

    NO = {} # [-]
    for i in [41,42,43]:
        l = lines[i].strip().split(" ", 1)
        NO[l[0]] = float(l[1].strip())

    NO2 = {} # [-]
    for i in [50,51,52]:
        l = lines[i].strip().split(" ", 1)
        NO2[l[0]] = float(l[1].strip())

    CH4 = {} # [-]
    for i in [59,60,61]:
        l = lines[i].strip().split(" ", 1)
        CH4[l[0]] = float(l[1].strip())
    
    CH = {} # [-]
    for i in [68,69,70]:
        l = lines[i].strip().split(" ", 1)
        CH[l[0]] = float(l[1].strip())
 
    CH2 = {} # [-]
    for i in [77,78,79]:
        l = lines[i].strip().split(" ", 1)
        CH2[l[0]] = float(l[1].strip())

    CH3 = {} # [-]
    for i in [86,87,88]:
        l = lines[i].strip().split(" ", 1)
        CH3[l[0]] = float(l[1].strip())    
        
    C2H = {} # [-]
    for i in [95,96,97]:
        l = lines[i].strip().split(" ", 1)
        C2H[l[0]] = float(l[1].strip()) 
        
    C2H2 = {} # [-]
    for i in [104,105,106]:
        l = lines[i].strip().split(" ", 1)
        C2H2[l[0]] = float(l[1].strip()) 
        
    C2H3 = {} # [-]
    for i in [113,114,115]:
        l = lines[i].strip().split(" ", 1)
        C2H3[l[0]] = float(l[1].strip()) 
        
    C2H4 = {} # [-]
    for i in [122,123,124]:
        l = lines[i].strip().split(" ", 1)
        C2H4[l[0]] = float(l[1].strip()) 

    C2H5 = {} # [-]
    for i in [131,132,133]:
        l = lines[i].strip().split(" ", 1)
        C2H5[l[0]] = float(l[1].strip()) 
        
    C2H6 = {} # [-]
    for i in [140,141,142]:
        l = lines[i].strip().split(" ", 1)
        C2H6[l[0]] = float(l[1].strip()) 
        
    C3H7 = {} # [-]
    for i in [149,150,151]:
        l = lines[i].strip().split(" ", 1)
        C3H7[l[0]] = float(l[1].strip()) 
        
    C3H8 = {} # [-]
    for i in [158,159,160]:
        l = lines[i].strip().split(" ", 1)
        C3H8[l[0]] = float(l[1].strip()) 

    return {"O2":O2, "N2":N2, "CO":CO, "CO2":CO2, "NO":NO, "NO2":NO2, "CH4":CH4,
            "CH":CH, "CH2":CH2, "CH3":CH3, "C2H":C2H, "C2H2":C2H2, "C2H3":C2H3,
            "C2H4":C2H4, "C2H5":C2H5, "C2H6":C2H6, "C3H7":C3H7, "C3H8":C3H8}
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def X_unit(data):

    O2 = data["O2"]["p6"]*100 # [%]
    CO = data["CO"]["p6"]*1e6 # [ppm]
    CO2 = data["CO2"]["p6"]*100 # [%]
    NO = data["NO"]["p6"]*1e6 # [ppm]
    NO2 = data["NO2"]["p6"]*1e6 # [ppm]
    NOx = NO + NO2 # [ppm]
    CH4 = data["CH4"]["p6"]*1e6 # [ppm]
    
    CxHy = (data["CH4"]["p6"] + data["CH"]["p6"] + data["CH2"]["p6"] + \
           data["CH3"]["p6"] + 2*data["C2H"]["p6"] + 2*data["C2H2"]["p6"] + \
           2*data["C2H3"]["p6"] + 2*data["C2H4"]["p6"] + 2*data["C2H5"]["p6"] + \
           2*data["C2H6"]["p6"] + 3*data["C3H7"]["p6"] + 3*data["C3H8"]["p6"])*1e6
           # [ppm]
            
    return {"O2 %":round(O2,2), "CO ppm":round(CO,2), "CO2 %":round(CO2,2),
            "NO ppm":round(NO,2), "NO2 ppm":round(NO2,2), "NOx ppm":round(NOx,2), 
            "CH4 ppm":round(CH4,2), "CxHy ppm":round(CxHy,2)}
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Index from path]-------
def X_all(path):
    xi = read_X(path)
    xu = X_unit(xi)
    return xu
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Indices from paths]----
def X_super(paths): # list of paths for each case
    R = []
    for path in paths:
        xi = read_X(path)
        xu = X_unit(xi)
        R.append(xu)
        
    O2 = [value for data in R 
                for key, value in data.items() if key == "O2 %"]
    CO = [value for data in R
                for key, value in data.items() if key == "CO ppm"]
    CO2 = [value for data in R
                for key, value in data.items() if key == "CO2 %"]
    NO = [value for data in R
                for key, value in data.items() if key == "NO ppm"]
    NO2 = [value for data in R
                for key, value in data.items() if key == "NO2 ppm"]
    NOx = [value for data in R
                for key, value in data.items() if key == "NOx ppm"]
    CH4 = [value for data in R
                for key, value in data.items() if key == "CH4 ppm"]
    CxHy = [value for data in R
                for key, value in data.items() if key == "CxHy ppm"]
    
    results = {"O2 %": O2, "CO ppm": CO, "CO2 %": CO2, "NO ppm": NO, 
               "NO2 ppm": NO2, "NOx ppm":NOx, "CH4 ppm": CH4, "CxHy ppm":CxHy}
    
    return results
#------------------------------------------------------------------------------

#---------------------------------------------[Usage]--------------------------
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
#------------------------------------------------------------------------------