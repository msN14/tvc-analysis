"""
*******************************************************************************
                    EMISSION INDEX FROM SIMULATION DATA (Bi/Ci STEADY)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Read Point Data from Fluent .srp file
    2. The .srp file data is used for EI calculations
    
    Include functions from developement .py files, listed below :
        
        analysis_Bi.py + read_mdot_species.py + read_x_species.py
...............................................................................
REFERENCES:

...............................................................................
DEVELOPER INFORMATION: NISANTH M. S,
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------
import emissionO2basis as eo2b
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# Species Flow Rate (mdot)                                  [g/s]            
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def read_mdot(fileLocation):

    with open(fileLocation, "r") as f:
        lines = f.readlines()

    O2 = {} # [g/s]
    for i in [5,6,7]:
        l = lines[i].strip().split(" ", 1)
        O2[l[0]] = float(l[1].strip())
        
    N2 = {} # [g/s]
    for i in [14,15,16]:
        l = lines[i].strip().split(" ", 1)
        N2[l[0]] = float(l[1].strip())

    CO = {} # [g/s]
    for i in [23,24,25]:
        l = lines[i].strip().split(" ", 1)
        CO[l[0]] = float(l[1].strip())
    
    NO = {} # [g/s]
    for i in [32,33,34]:
        l = lines[i].strip().split(" ", 1)
        NO[l[0]] = float(l[1].strip())

    NO2 = {} # [g/s]
    for i in [41,42,43]:
        l = lines[i].strip().split(" ", 1)
        NO2[l[0]] = float(l[1].strip())

    CH4 = {} # [g/s]
    for i in [50,51,52]:
        l = lines[i].strip().split(" ", 1)
        CH4[l[0]] = float(l[1].strip())

    CH = {} # [g/s]
    for i in [59,60,61]:
        l = lines[i].strip().split(" ", 1)
        CH[l[0]] = float(l[1].strip())
    
    CH2 = {} # [g/s]
    for i in [68,69,70]:
        l = lines[i].strip().split(" ", 1)
        CH2[l[0]] = float(l[1].strip())
 
    CH3 = {} # [g/s]
    for i in [77,78,79]:
        l = lines[i].strip().split(" ", 1)
        CH3[l[0]] = float(l[1].strip())

    C2H = {} # [g/s]
    for i in [86,87,88]:
        l = lines[i].strip().split(" ", 1)
        C2H[l[0]] = float(l[1].strip())    
        
    C2H2 = {} # [g/s]
    for i in [95,96,97]:
        l = lines[i].strip().split(" ", 1)
        C2H2[l[0]] = float(l[1].strip()) 
        
    C2H3 = {} # [g/s]
    for i in [104,105,106]:
        l = lines[i].strip().split(" ", 1)
        C2H3[l[0]] = float(l[1].strip()) 
        
    C2H4 = {} # [g/s]
    for i in [113,114,115]:
        l = lines[i].strip().split(" ", 1)
        C2H4[l[0]] = float(l[1].strip()) 
        
    C2H5 = {} # [g/s]
    for i in [122,123,124]:
        l = lines[i].strip().split(" ", 1)
        C2H5[l[0]] = float(l[1].strip()) 

    C2H6 = {} # [g/s]
    for i in [131,132,133]:
        l = lines[i].strip().split(" ", 1)
        C2H6[l[0]] = float(l[1].strip()) 
        
    C3H7 = {} # [g/s]
    for i in [140,141,142]:
        l = lines[i].strip().split(" ", 1)
        C3H7[l[0]] = float(l[1].strip()) 
        
    C3H8 = {} # [g/s]
    for i in [149,150,151]:
        l = lines[i].strip().split(" ", 1)
        C3H8[l[0]] = float(l[1].strip()) 

    return {"O2":O2, "N2":N2, "CO":CO, "NO":NO, "NO2":NO2, "CH4":CH4,
            "CH":CH, "CH2":CH2, "CH3":CH3, "C2H":C2H, "C2H2":C2H2, "C2H3":C2H3,
            "C2H4":C2H4, "C2H5":C2H5, "C2H6":C2H6, "C3H7":C3H7, "C3H8":C3H8}
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def read_X(fileLocation):

    with open(fileLocation, "r") as f:
        lines = f.readlines()

    O2 = {} # [g/s]
    for i in [5,6,7]:
        l = lines[i].strip().split(" ", 1)
        O2[l[0]] = float(l[1].strip())
        
    N2 = {} # [g/s]
    for i in [14,15,16]:
        l = lines[i].strip().split(" ", 1)
        N2[l[0]] = float(l[1].strip())

    NO = {} # [g/s]
    for i in [23,24,25]:
        l = lines[i].strip().split(" ", 1)
        NO[l[0]] = float(l[1].strip())
    
    NO2 = {} # [g/s]
    for i in [32,33,34]:
        l = lines[i].strip().split(" ", 1)
        NO2[l[0]] = float(l[1].strip())

    CH4 = {} # [g/s]
    for i in [41,42,43]:
        l = lines[i].strip().split(" ", 1)
        CH4[l[0]] = float(l[1].strip())

    CO = {} # [g/s]
    for i in [50,51,52]:
        l = lines[i].strip().split(" ", 1)
        CO[l[0]] = float(l[1].strip())

    CO2 = {} # [g/s]
    for i in [59,60,61]:
        l = lines[i].strip().split(" ", 1)
        CO2[l[0]] = float(l[1].strip())


    return {"O2":O2, "N2":N2, "NO":NO, "NO2":NO2, "CH4":CH4,
            "CO":CO, "CO2":CO2}
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def EI(data):
    mdot_fuel = (data["CH4"]["cavity_inlet"] + data["CH4"]["main_inlet"]) # [g/s]
    
    EICO = (abs(data["CO"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    
    EICH4 = (abs(data["CH4"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    
    # Need to check direct addition
    mdot_CxHy = data["CH4"]["outlet"] + data["CH"]["outlet"] + data["CH2"]["outlet"]+\
                data["CH3"]["outlet"] + data["C2H"]["outlet"] + data["C2H2"]["outlet"]+\
                data["C2H3"]["outlet"]+ data["C2H4"]["outlet"]+ data["C2H5"]["outlet"]+\
                data["C2H6"]["outlet"]+data["C3H7"]["outlet"]+data["C3H8"]["outlet"]
                
    EICxHy = (abs(mdot_CxHy)/mdot_fuel)*1000 # [g/Kg Fuel]
    
    EINO = (abs(data["NO"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    EINO2 = (abs(data["NO2"]["outlet"])/mdot_fuel)*1000 # [g/Kg Fuel]
    mdot_NOx = abs(data["NO"]["outlet"]) + abs(data["NO2"]["outlet"])
    EINOx = (mdot_NOx/mdot_fuel)*1000 # [g/Kg Fuel]
    
    Hc = 50e6 # Lower heating value of the fuel (LHV)  
    
    # Equation for combustion efficiency calculation
    eff1=(1-(10109*(EICO/Hc))-(EICH4/1000))*100 # CH4 Basis
    
    eff2=(1-(10109*(EICO/Hc))-(EICxHy/1000))*100 # CxHy Basis

    return {"EICO":round(EICO,2), "EICH4":round(EICH4,2), 
            "EICxHy":round(EICxHy,2), "EINO":round(EINO,2), 
            "EINO2":round(EINO2,2), "EINOx":round(EINOx,2), 
            "Eff1":round(eff1,2), "Eff2":round(eff2,2)}
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def X_unit(data):

    O2 = data["O2"]["outlet"]*100 # [%]
    CO = data["CO"]["outlet"]*1e6 # [ppm]
    NO = data["NO"]["outlet"]*1e6 # [ppm]
    NO2 = data["NO2"]["outlet"]*1e6 # [ppm]
    NOx = NO + NO2
    CO2 = data["CO2"]["outlet"]*100 # [%]
    CH4 = data["CH4"]["outlet"]*1e6 # [ppm]

    return {"O2":round(O2,2), "CO":round(CO,2), "NO":round(NO,2),
            "NO2":round(NO2,2), "NOx":round(NOx,2), "CO2":round(CO2,2),
            "CH4":round(CH4,2)}
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Index from path]-------
def EI_all(path):
    mdots = read_mdot(path)
    EIs = EI(mdots)
    return EIs
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Index from path]-------
def X_all(path):
    xi = read_X(path)
    xu = X_unit(xi)
    return xu
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Indices from paths]----
def EI_super(paths): # list of paths for each case
    R = []
    for path in paths:
        mdots = read_mdot(path)
        EIs = EI(mdots)
        R.append(EIs)
        
    EICOs = [value for data in R 
            for key, value in data.items() if key == "EICO"]
    EINOs = [value for data in R  
             for key, value in data.items() if key == "EINO"]
    EINOxs = [value for data in R  
            for key, value in data.items() if key == "EINOx"]
    EICH4s = [value for data in R 
                for key, value in data.items() if key == "EICH4"]
    EICxHys = [value for data in R 
                for key, value in data.items() if key == "EICxHy"]
    Effs1 = [value for data in R 
                for key, value in data.items() if key == "Eff1"]
    Effs2 = [value for data in R 
                for key, value in data.items() if key == "Eff2"]
    
    results = {"EICO":EICOs, "EICH4":EICH4s, "EINO":EINOs, "EICxHy":EICxHys,
               "EINOx":EINOxs, "Eff1":Effs1, "Eff2":Effs2}
    return results # each element contain result for all cases given in paths
#------------------------------------------------------------------------------

#---------------------------------------------[Emission Indices from paths]----
def X_super(paths): # list of paths for each case
    R = []
    for path in paths:
        xi = read_X(path)
        xu = X_unit(xi)
        o2basis = eo2b.O2Basis(xu['O2'], xu['CO'], xu['NOx'], xu['NO'], 
                               xu['NO2'], xu['CO2'], xu['CH4'])
        R.append(o2basis)
        
    CO_15 = [value for data in R 
                for key, value in data.items() if key == "CO 15%-O2"]
    NOx_15 = [value for data in R
                for key, value in data.items() if key == "NOx 15%-O2"]
    NO_15 = [value for data in R
                for key, value in data.items() if key == "NO 15%-O2"]
    NO2_15 = [value for data in R
                for key, value in data.items() if key == "NO2 15%-O2"]
    CO2_15 = [value for data in R
                for key, value in data.items() if key == "CO2 15%-O2"]
    CxHy_15 = [value for data in R
                for key, value in data.items() if key == "CxHy 15%-O2"]
    
    results = {"CO 15%-O2": CO_15, "NOx 15%-O2": NOx_15, "NO 15%-O2": NO_15,
               "NO2 15%-O2": NO2_15, "CO2 15%-O2": CO2_15, 
               "CxHy 15%-O2": CxHy_15}
    
    return results
#------------------------------------------------------------------------------

#---------------------------------------------[Usage]--------------------------
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
#------------------------------------------------------------------------------