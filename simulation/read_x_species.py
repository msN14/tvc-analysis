"""
*******************************************************************************
                        READING POINT DATA FROM SRP FILE (B1 STEADY)
...............................................................................
DESCRIPTION OF THE FUNCTION:
    1. Read Point Data from Fluent .srp file
...............................................................................
REFERENCES:

...............................................................................
DEVELOPER INFORMATION: NISANTH M. S,
*******************************************************************************
"""
#---------------------------------------------[Imports]------------------------

#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# Species Flow Rate (mdot)                                  [g/s]            
#------------------------------------------------------------------------------

#---------------------------------------------[readSRP]------------------------
def readSRP(fileLocation):

    with open(fileLocation, "r") as f:
        lines = f.readlines()

    O2 = {} # [g/s]
    for i in [5,6,7]:
        l = lines[i].strip().split(" ", 1)
        O2[l[0]] = float(l[1].strip())
        
    N2 = {} # [g/s]
    for i in [14,15,16]:
        l = lines[i].strip().split(" ", 1)
        N2[l[0]] = float(l[1].strip())

    NO = {} # [g/s]
    for i in [23,24,25]:
        l = lines[i].strip().split(" ", 1)
        NO[l[0]] = float(l[1].strip())
    
    NO2 = {} # [g/s]
    for i in [32,33,34]:
        l = lines[i].strip().split(" ", 1)
        NO2[l[0]] = float(l[1].strip())

    CH4 = {} # [g/s]
    for i in [41,42,43]:
        l = lines[i].strip().split(" ", 1)
        CH4[l[0]] = float(l[1].strip())

    CO = {} # [g/s]
    for i in [50,51,52]:
        l = lines[i].strip().split(" ", 1)
        CO[l[0]] = float(l[1].strip())

    CO2 = {} # [g/s]
    for i in [59,60,61]:
        l = lines[i].strip().split(" ", 1)
        CO2[l[0]] = float(l[1].strip())


    return {"O2":O2, "N2":N2, "NO":NO, "NO2":NO2, "CH4":CH4,
            "CO":CO, "CO2":CO2}
#------------------------------------------------------------------------------

#---------------------------------------------[Usage]--------------------------
# fileLocation = "./test.srp"
# emission = readSRP(fileLocation)
#------------------------------------------------------------------------------