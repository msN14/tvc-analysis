# TVC Analysis

Trapped Vortex Combustor (TVC) uses physical cavity to stabilize flame, this pilot flame stabilizes the main flame in the combustor. The architecture is an example of staged combustion system that can be utilized for RQL and Lean Staged (LP) operation.


## Purpose

The purpose of this project are,

1. Calculation of basic flow and chemical parameters of TVC.
2. Analysis of experimental measurements (temperature, pollutant emission and dynamic pressure).
3. Analysis of numerical simulaton results. 

## Structure

Object oriented programming in Python is implemented in this project. All the functions are grouped into 5 categories.

1. root
    - basic functions to set conditions and calculate properties of the combustor. 
    - 4 parameters are required to set the state of the TVC system
2. general
3. reporting
4. simulation
5. plotting

## Usage

- Place a .pth file with all the directories in the system path

```
import sys
sys.path

```

- Use example files provided in the code 

- root function can be used to set the operating conditions of the TVC. Three modes are available, form1, form2, and form3.
- root functions include simulation functions to form the simulation object and manipulate the results.
