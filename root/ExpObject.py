"""
*******************************************************************************
                        EXPERIMENT DATA TOOLS FOR TVC
...............................................................................
DESCRIPTION:
    1. Defining condition id, mfc, temperature and pressure data to 
       experiment object
    2. Processing tools like radiation correction, emission data processing
       etc are applied to the measured data
    3. Linking nonlinear time series analysis tools for dynamic pressure 
       data analysis
...............................................................................
REFERENCES:
...............................................................................
DEVELOPER: NISANTH M. S, 
*******************************************************************************
"""
##---------------------------------------------[Imports]-----------------------
import numpy as np
import radiationCorrection as rc
import emissionO2basis as eob
import emissionIndex as ei

import temperatureTime as tT
import mfcTime as mT
import pressureTime as pT
import emissionTime as eT

import reportExcel as re
import reportExcelCombine as rec
import xlrd as xl # Reading xlsx file
xl.xlsx.ensure_elementtree_imported(False, None)
xl.xlsx.Element_has_iter = True
# import openpyxl as oxl # Reading xlsx file
import xlsxwriter as xw # Writing to xlsx file

# systematic errors
import errorFunctions as ef
#------------------------------------------------------------------------------

##---------------------------------------------[Variables and Units]-----------
# geometric parameters [L, D, W, H, t]      [mm]
# pressure (P)                              [Pa]            
# temperature (T)                           [K]
# main velocity (Vx)                        [m/s]
# cavity velocity (Ux)                      [m/s]
# jet momentum flux ratio (J)               [-]
# equivalence ratio of cavity inlet (ERC)   [-]
# equivalence ratio of main inlet (ERO)     [-]
# overall equivalence ratio (ERO)           [-]
#------------------------------------------------------------------------------
  
#:::::::::::::::::::::::::::::::::::::::::::::[Input/Output Pass]::::::::::::::
    
class IO:

    def Load(caseName,fileName):
        """
        Convert Excel data to numpy array data.

        Parameters
        ----------
        caseName : str
            Folder name of the experiment data.
        fileName : str
            Name of the file.

        Returns
        -------
        dataDictionary : dict
            Sheet columns with {'title':np.array([values])}.

        """
        loc = (caseName+'/'+fileName) #Name of the CASE and FILE
        wb = xl.open_workbook(loc) #Opening Workbook
        sheet = wb.sheet_by_index(0) #Opening Specific Excel workseet
        dataArray = [] #Return Variable colum[0]=heading and colum[1:]=data
        for i in range(sheet.ncols):
            ElementColumn = []
            for j in range(sheet.nrows):
                Element = sheet.cell_value(j, i)
                ElementColumn.append(Element)
            dataArray.append(ElementColumn)
        dataDictionary = {x[0]: np.array(x[1:]) for x in dataArray} #Dictionary
        # Return=[dataArray,LimitOfData,dictionaryFormat]
        # return(dataArray,[sheet.nrows,sheet.ncols],dataDictionary)
        return dataDictionary  
    
    def createExpObject(errorFileName, caseName):
        """
        Formulating objects based on the loaded excel files.

        Parameters
        ----------
        caseName : str
            Folder name of the experiment data.

        Returns
        -------
        object
            Experiment object with all linked data.

        """
        print("\n--------------------------------------------------------------")
        print("CREATING EXPERIMENT OBJECT\n")
        print(">> Reading Random Error File")
        err=Errors(IO.Load(".",errorFileName+".xlsx"))
        print(">> Reading Conditions")
        c=Conditions(IO.Load(caseName,"conditions.xlsx"))
        print(">> Reading MFC Data")
        m=Mfc(IO.Load(caseName,"mfcOutput.xlsx"))
        print(">> Reading Pressure Data")
        p=Pressure(IO.Load(caseName,"pressure.xlsx"), err)
        print(">> Reading Temperature Data")
        t=Temperature(IO.Load(caseName,"temperature.xlsx"), c, p, err)
        print(">> Reading Emission Data")
        e=Emission(IO.Load(caseName,"emission.xlsx"), err)
        print("--------------------------------------------------------------")
        return Experiment(caseName, err, c, m, t, p, e)
    
    def plotsAndReport(obj, Lm, Lp, Lt, Le):
        """
        Plotting and Reporting with specified limits.

        Parameters
        ----------
        obj : object
            Experiment object.
        Lm : list
            List for selecting a portion in the MFC data.
            List = [startTime, endTime].
        Lp : list
            List for selecting a portion in the pressure data.
            List = [startTime, endTime].
        Lt : list
            List for selecting a portion in the temperature data.
            List = [startTime, endTime].
        Le : list
            List for selecting a portion in the emission data.
            List = [startTime, endTime].

        Returns
        -------
        None.

        """
        print("\n--------------------------------------------------------------")
        print("[1] PROCESSING CONDITION AND MFC DATA")
        obj.conditions.processing()
        obj.mfc.processing(Lm)
        print("\n--------------------------------------------------------------")
        print("[2] PROCESSING PRESSURE DATA")
        obj.pressure.processing(Lp)
        print("\n--------------------------------------------------------------")
        print("[3] PROCESSING TEMPERATURE DATA")
        obj.temperature.processing(Lt)
        print("\nOperating Pressure (obj.pressure) :", 
                                                obj.pressure.operatingPressure)
        obj.temperature.tempCorrection(obj.pressure.operatingPressure)
        print("\n--------------------------------------------------------------")
        print("[4] PROCESSING EMISSION DATA")
        obj.emission.refresh()
        obj.emission.processing(Le)
        obj.emission.o2Report()
        obj.emission.eiReport()
        
        mT.mfcTimePlot(obj.id, obj.mfc.dataPlot,obj.mfc.dataReport, Lm)
        pT.pressureTimePlot(obj.id, obj.pressure.dataPlot, 
                            obj.pressure.dataReport, Lp)
        tT.tempTimePlot(obj.id, obj.temperature.dataPlot,
                        obj.temperature.dataReport, Lt)
        eT.emissionTimePlot(obj.id, obj.emission.dataPlot, 
                            obj.emission.dataReport, Le)
        print("\n--------------------------------------------------------------")
        print("[5] CREATING EXCEL REPORT")
        re.reportExcel( obj.id, obj.conditions.dataReport,
                obj.mfc.dataReport, obj.temperature.dataReport,
                obj.temperature.corrected, obj.pressure.dataReport,
                obj.emission.dataReport, obj.emission.o2result, 
                obj.emission.result )
        return
    
    def plotsAndReportWithErrors(obj, Lm, Lp, Lt, Le, suffix1, suffix2, display):
        """
        Plotting and Reporting with specified limits.

        Parameters
        ----------
        obj : object
            Experiment object.
        Lm : list
            List for selecting a portion in the MFC data.
            List = [startTime, endTime].
        Lp : list
            List for selecting a portion in the pressure data.
            List = [startTime, endTime].
        Lt : list
            List for selecting a portion in the temperature data.
            List = [startTime, endTime].
        Le : list
            List for selecting a portion in the emission data.
            List = [startTime, endTime].

        Returns
        -------
        None.

        """
        print("\n--------------------------------------------------------------")
        # print("[0] READING RANDOM ERROR")
        # obj.errors()
        print("[1] PROCESSING CONDITION AND MFC DATA")
        obj.conditions.processing()
        obj.mfc.processing(Lm)
        print("\n--------------------------------------------------------------")
        print("[2] PROCESSING PRESSURE DATA")
        obj.pressure.processing(Lp)
        print("\n--------------------------------------------------------------")
        print("[3] PROCESSING TEMPERATURE DATA")
        obj.temperature.processing(Lt)
        print("\nOperating Pressure (obj.pressure) :", 
                                                obj.pressure.operatingPressure)
        obj.temperature.tempCorrection(obj.pressure.operatingPressure)
        print("\n--------------------------------------------------------------")
        print("[4] PROCESSING EMISSION DATA")
        obj.emission.refresh()
        obj.emission.processing(Le)
        obj.emission.o2Report()
        obj.emission.eiReport()
        
        mT.mfcTimePlot(obj.id, obj.mfc.dataPlot,obj.mfc.dataReport, Lm, display)
        pT.pressureTimePlot(obj.id, obj.pressure.dataPlot, 
                            obj.pressure.dataReport, Lp, display)
        tT.tempTimePlot(obj.id, obj.temperature.dataPlot,
                        obj.temperature.dataReport, Lt, display)
        eT.emissionTimePlot(obj.id, obj.emission.dataPlot, 
                            obj.emission.dataReport, Le, display)
        print("\n--------------------------------------------------------------")
        print("[5] CREATING EXCEL REPORT")
        # re.reportExcel( obj.id, obj.conditions.dataReport,
        #         obj.mfc.dataReport, obj.temperature.dataReport,
        #         obj.temperature.corrected, obj.pressure.dataReport,
        #         obj.emission.dataReport, obj.emission.o2DataReport, 
        #         obj.emission.eiDataReport )
        
        re.reportExcelAll(obj.id, obj.conditions.dataReport,
                obj.mfc.dataReport, obj.temperature.dataReport,
                obj.temperature.corrected,obj.temperature.errorReport, 
                obj.pressure.dataReport,obj.pressure.errorReport,
                obj.emission.dataReport,obj.emission.errorReport,
                obj.emission.o2AllReport[0], obj.emission.o2AllReport[1],
                obj.emission.eiAllReport[0], obj.emission.eiAllReport[1],
                suffix1)
        re.reportExcelAll(obj.id, obj.conditions.dataReport,
                obj.mfc.dataReport, obj.temperature.dataReport,
                obj.temperature.corrected,obj.temperature.sysErrorReport, 
                obj.pressure.dataReport,obj.pressure.sysErrorReport,
                obj.emission.dataReport,obj.emission.sysErrorReport,
                obj.emission.o2AllReport[0], obj.emission.o2AllSysReport[1],
                obj.emission.eiAllReport[0], obj.emission.eiAllSysReport[1],
                suffix2)
        return
    
    def combineReport(objs, suffix):
        """
        Combine results of multiple experiment object to single excel file.

        Parameters
        ----------
        objs : list
            List of objects.
            List = [E1, E2, E3].

        Returns
        -------
        None.

        """
        workbook = xw.Workbook('results-'+suffix+'.xlsx')
        worksheet = workbook.add_worksheet()
        
        cell_format = workbook.add_format()
        cell_format.set_align('center')
        cell_format.set_align('vcenter')
        cell_format.set_text_wrap()
        worksheet.set_row(0, 50)
        row = 1
        
        # Combining Data of RE and SE
        if suffix == 'RE+SE':
            for obj in objs:
                print("\nAdding ID:",obj.id,"-> row=",row)
                rec.reportExcelAll(worksheet,cell_format,
                                row, obj.id, obj.conditions.dataReport,
                                obj.mfc.dataReport, obj.temperature.dataReport,
                                obj.temperature.corrected,obj.temperature.errorReport, 
                                obj.pressure.dataReport,obj.pressure.errorReport,
                                obj.emission.dataReport,obj.emission.errorReport,
                                obj.emission.o2AllReport[0], obj.emission.o2AllReport[1],
                                obj.emission.eiAllReport[0], obj.emission.eiAllReport[1])
                row = row + 1
        # Combining Data of SE only
        else :
            for obj in objs:
                print("\nAdding ID:",obj.id,"-> row=",row)
                rec.reportExcelAll(worksheet,cell_format,
                                row, obj.id, obj.conditions.dataReport,
                                obj.mfc.dataReport, obj.temperature.dataReport,
                                obj.temperature.corrected,obj.temperature.sysErrorReport, 
                                obj.pressure.dataReport,obj.pressure.sysErrorReport,
                                obj.emission.dataReport,obj.emission.sysErrorReport,
                                obj.emission.o2AllSysReport[0], obj.emission.o2AllSysReport[1],
                                obj.emission.eiAllSysReport[0], obj.emission.eiAllSysReport[1])
                row = row + 1
            
        workbook.close()
        return
        
#:::::::::::::::::::::::::::::::::::::::::::::[Experiment Object]::::::::::::::
        
class Experiment:
    
    def __init__(self,id,errors,conditions,mfc,temperature,pressure,emission):
        """
        Initilizing experiment object.

        Parameters
        ----------
        id : str
            String of experiment condition.
            Example : 'P2T300LD1.0t2V10J3.4ERC2.0ERM0.2DA1.0'
        conditions : object
            Object to pass condition data.
        mfc : object
            Object to pass MFC data.
        temperature : object
            Object to pass temperature data.
        pressure : object
            Object to pass pressure data.
        emission : object
            Object to pass emission data.

        Returns
        -------
        None.

        """
        self.id = id
        self.errors = errors
        self.conditions = conditions
        self.mfc = mfc
        self.temperature = temperature
        self.pressure = pressure
        self.emission = emission
        
        return

#:::::::::::::::::::::::::::::::::::::::::::::[Reading Error Files]::::::::::::
class Errors:
    
    def __init__(self, errors):
        """
        Initializing condition object.

        Parameters
        ----------
        conditions : dict
            Dictionary data from Load() of condition excel data.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = errors
        
#:::::::::::::::::::::::::::::::::::::::::::::[Defining Operating Condition]:::   
        
class Conditions:
    
    def __init__(self, conditions):
        """
        Initializing condition object.

        Parameters
        ----------
        conditions : dict
            Dictionary data from Load() of condition excel data.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = conditions
    
    def processing(self, rowNumber=None):
        """
        Process condition excel data for report.

        Parameters
        ----------
        rowNumber : int, optional
            Row number to select a row in excel file. The default is None.

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        
        if rowNumber == None:
            rowNumber = 3
            print("\nCONDITIONS : Row Number is not given, 3 is default")
        self.dataReport = {key:value[rowNumber] 
                           for key, value in self.data.items()}
        del self.dataReport['Time']
        self.rowNum = rowNumber
        
        return self.dataReport
    
#:::::::::::::::::::::::::::::::::::::::::::::[MFC data in the object]:::::::::    

class Mfc:
    
    def __init__(self,mfc):
        """
        Initializing Mfc object.

        Parameters
        ----------
        mfc : dict
            Dictionary data from Load() of MFC excel data.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = mfc
    
    def processing(self,R=None):
        """
        Process MFC excel data for report.
        Mean over the time range.

        Parameters
        ----------
        R : list, optional
            List of upper and lower limit. The default is None.
            List = [startTime, endTime].

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        
        if R == None:
            self.dataReport = {key:round(np.mean(value),2) 
                              for key, value in self.data.items()}
            print("MFC : Range [R1,R2] is not given, complete average")
        else:
            self.dataReport = {key:round(np.mean(value[R[0]:R[1]]),2) 
                              for key, value in self.data.items()}
        del self.dataReport['Time']
        self.dataPlot = {key:value for key, value in self.data.items()}
        del self.dataPlot['Time']
        Length = len(next(iter(self.dataPlot.values())))
        self.dataPlot['Time'] = np.linspace(0,Length-1,Length)
        self.meanRange = R
        
        return self.dataReport
    
#:::::::::::::::::::::::::::::::::::::::::::::[Temperature Object]:::::::::::::
    
class Temperature:
    
    def __init__(self,temp, conditions, pressure, errs):
        """
        Initializing temperature object.

        Parameters
        ----------
        temp : dict
            Dictionary data from Load() of condition excel data.
        conditions : object
            Passing information from condition to temperature object.
            Flow rate information for radiation correction.
        pressure : object
            Passing information from pressure to temperature object.
            Operating pressure for radiation correction.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = temp
        self.condition = conditions.processing()
        # self.condition = conditions.dataReport
        print("TEMPERATURE : Conditions for row=1")
        self.operatingPressure = pressure.processing()['P7 (bar)']*1e5
        # self.operatingPressure = pressure.operatingPressure
        print("TEMPERATURE : Operating Pressure is averaged over all data")
        
        print("Transfering error data to temperature")
        self.errors = errs
    
    def processing(self, R=None):
        """
        Process temperature excel data for report.
        Mean over the time range.

        Parameters
        ----------
        R : list, optional
            List of upper and lower limit. The default is None.
            List = [startTime, endTime].

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        self.dataPlot = {key:value for key, value in self.data.items()}
        Step = self.dataPlot['Relative Time (s)'][1]
        del self.dataPlot['Time']
        del self.dataPlot['Relative Time (s)']
        Length=len(next(iter(self.dataPlot.values())))
        self.dataPlot['Time'] = np.arange(0, Step*Length, Step)
        
        if R == None:
            self.dataReport = {key:round(np.mean(value),2) 
                              for key, value in self.data.items()}
            print("TEMPERATURE : Range [R1,R2] is not given, complete average")
            
        else: # map from time value to actual list location ->
            A1, A2 = np.where(self.dataPlot['Time']<R[0])[0][-1], \
                     np.where(self.dataPlot['Time']<R[1])[0][-1]
                     
            self.dataReport = {key:round(np.mean(value[A1:A2]),2) 
                               for key, value in self.data.items()}
        del self.dataReport['Time']
        del self.dataReport['Relative Time (s)']
        self.meanRange = R
        
        return self.dataReport 
    
    def tempCorrection(self, operatingPressure=None):
        """
        Radiation correction of temperature data.

        Parameters
        ----------
        operatingPressure : float, optional
            Operating pressure of the combustor. The default is None.

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        
        # Operating Pressure of Temperature Class is upated with
        # proper average (range) from pressure class rather than using
        # full average at the class definition of Temperature
        
        self.operatingPressure = operatingPressure
        
        self.corrected={}
        
        Tambient = 300 # [K]
        mdot_tvc = (self.condition['Main Air (g/s)']  + \
                   self.condition['Main Fuel (g/s)'] + \
                   self.condition['Caviy Air (g/s)'] + \
                   self.condition['Cavity Fuel (g/s)']) / 1000 # [kg/s]
            
        print("\nOperating Pressure (obj.temperature) = ", 
                                      round(self.operatingPressure,0), "[Pa]")
        
        for key,value in self.dataReport.items():
            if value >=300:
                print("\n",key,value,"---> Correcting")
                self.corrected[key+' corrected'] = \
                round(rc.RadiationCorrection
                      (value, self.operatingPressure,
                       self.condition['ERO (-)'], mdot_tvc),1)
            if value>200 and value<300:
                print("\n",key,value,"---> No Correction")
                self.corrected[key+' corrected']= \
                round(rc.RadiationCorrection
                      (value, self.operatingPressure,
                       self.condition['ERO (-)'], mdot_tvc),1)
            if key=='PF (-)':
                Tmax = max(self.corrected['T5 (K) corrected'],
                           self.corrected['T6 (K) corrected'],
                           self.corrected['T7 (K) corrected'])
                Tave=np.mean([self.corrected['T5 (K) corrected'],
                              self.corrected['T6 (K) corrected'],
                              self.corrected['T7 (K) corrected']])
                self.corrected[key+' corrected'] = \
                    round((Tmax-Tave)/(Tave-Tambient),2)
                    
        # ERROR ---------------------------------------------------------------          
        # reading the random errors as fractional error, not %
        re_T = np.array([self.errors.data['T1 (%)'][0]/100,
                         self.errors.data['T2 (%)'][0]/100,
                         self.errors.data['T3 (%)'][0]/100,
                         self.errors.data['T4 (%)'][0]/100,
                         self.errors.data['T5 (%)'][0]/100,
                         self.errors.data['T6 (%)'][0]/100,
                         self.errors.data['T7 (%)'][0]/100,
                         self.errors.data['T8 (%)'][0]/100])
        
        # no random error
        # re0_T = np.zeros(8)
       
        # reading the systematic errors as fractional error, not %
        sys_T = np.array([ef.thermoErr(self.corrected['T1 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T2 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T3 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T4 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T5 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T6 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T7 (K) corrected'], 'K'),
                          ef.thermoErr(self.corrected['T8 (K) corrected'], 'K')])
        
        # total fractional error, not %
        total_T = np.sqrt(np.square(re_T) + np.square(sys_T))
        # total fractional error, without random error, not %
        total0_T = sys_T
        
        self.errorReport = {"T1 (K) Error": round(total_T[0]*self.corrected['T1 (K) corrected'],2), 
                            "T2 (K) Error": round(total_T[1]*self.corrected['T2 (K) corrected'],2),
                            "T3 (K) Error": round(total_T[2]*self.corrected['T3 (K) corrected'],2), 
                            "T4 (K) Error": round(total_T[3]*self.corrected['T4 (K) corrected'],2),
                            "T5 (K) Error": round(total_T[4]*self.corrected['T5 (K) corrected'],2), 
                            "T6 (K) Error": round(total_T[5]*self.corrected['T6 (K) corrected'],2),
                            "T7 (K) Error": round(total_T[6]*self.corrected['T7 (K) corrected'],2), 
                            "T8 (K) Error": round(total_T[7]*self.corrected['T8 (K) corrected'],2)}
        
        self.sysErrorReport = {"T1 (K) Error": round(total0_T[0]*self.corrected['T1 (K) corrected'],2), 
                               "T2 (K) Error": round(total0_T[1]*self.corrected['T2 (K) corrected'],2),
                               "T3 (K) Error": round(total0_T[2]*self.corrected['T3 (K) corrected'],2), 
                               "T4 (K) Error": round(total0_T[3]*self.corrected['T4 (K) corrected'],2),
                               "T5 (K) Error": round(total0_T[4]*self.corrected['T5 (K) corrected'],2), 
                               "T6 (K) Error": round(total0_T[5]*self.corrected['T6 (K) corrected'],2),
                               "T7 (K) Error": round(total0_T[6]*self.corrected['T7 (K) corrected'],2), 
                               "T8 (K) Error": round(total0_T[7]*self.corrected['T8 (K) corrected'],2)}
        
        # Pattern Factor ERROR ------------------------------------------------
        Profile = {'T5 (K) corrected':self.corrected['T5 (K) corrected'],
                   'T6 (K) corrected':self.corrected['T6 (K) corrected'],
                   'T7 (K) corrected':self.corrected['T7 (K) corrected']} 
        
        s_PF = ef.PFerror(Profile, self.errorReport)
        s0_PF = ef.PFerror(Profile, self.sysErrorReport)
        
        self.errorReport.update({"PF (-) Error": round(s_PF,2)})
        self.sysErrorReport.update({"PF (-) Error": round(s0_PF,2)})
        
        return self.corrected
    
#:::::::::::::::::::::::::::::::::::::::::::::[Presssure Data Object]::::::::::
    
class Pressure:
    
    def __init__(self, pressure, errs):
        """
        Initializing pressure object.

        Parameters
        ----------
        pressure : dict
            Dictionary data from Load() of pressure excel data.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = pressure
        print("Transfering error data to pressure")
        self.errors = errs
    
    def processing(self, R=None):
        """
        Process pressure excel data for report.
        Mean over the time range.

        Parameters
        ----------
        R : list, optional
            List of upper and lower limit. The default is None.
            List = [startTime, endTime].

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        self.dataPlot = {key:value for key, value in self.data.items()}
        Step = self.dataPlot['Relative Time (s)'][1]
        del self.dataPlot['Time']
        del self.dataPlot['Relative Time (s)']
        # del self.dataPlot['Dynamic Pressure Sensor P1'] # Comment for open Test
        Length = len(next(iter(self.dataPlot.values())))
        self.dataPlot['Time'] = np.arange(0, Step*Length, Step)
        
        if R == None:
            self.dataReport = {key:round(np.mean(value),2) 
                               for key, value in self.data.items()}
            print("PRESSURE : Range [R1,R2] is not given, complete average")
            
        else: # map from time value to actual list location ->
            A1, A2 = np.where(self.dataPlot['Time']<R[0])[0][-1], \
                     np.where(self.dataPlot['Time']<R[1])[0][-1]
                     
            self.dataReport = {key:round(np.mean(value[A1:A2]),2) 
                               for key, value in self.data.items()}
            
        del self.dataReport['Time']
        del self.dataReport['Relative Time (s)']   
        # del self.dataReport['Dynamic Pressure Sensor P1'] # Comment for open Test
        self.operatingPressure = self.dataReport['P7 (bar)']*1e5
        
        # Providing Proper Operating Pressure for other calculations
        # tempCorrection
        if self.operatingPressure < 101325:
            self.operatingPressure = 101325
        else:
            self.operatingPressure = self.operatingPressure
        
        self.meanRange = R
        
        # ERROR ---------------------------------------------------------------    
        # reading random fractional error # not %
        re_P7 = self.errors.data['P7 (%)'][0]/100
        
        # reading systematic fractional error # not %
        sys_P7 = ef.pressureErr(self.dataReport['P7 (bar)'], 'P7')
        
        # total fractional error, not %
        total_P = np.sqrt(np.square([re_P7]) + np.square([sys_P7]))
        total0_P = sys_P7
        
        print("TEST : ", total_P, total0_P)
        
        self.errorReport = {"P7 (bar) Error": total_P[0]*self.dataReport['P7 (bar)']}
        self.sysErrorReport = {"P7 (bar) Error": total0_P*self.dataReport['P7 (bar)']}
        
        # print("\n ALL Pressure :", re_P7, sys_P7, total_P)
        
        return self.dataReport
    
#:::::::::::::::::::::::::::::::::::::::::::::[Emission Data Object]:::::::::::    

class Emission:
    
    def __init__(self, emission, errs):
        """
        Initializing emission object.

        Parameters
        ----------
        emission : dict
            Dictionary data from Load() of emission excel data.

        Returns
        -------
        None.

        """
        # self.title = conditions.keys()
        # self.values = conditions.values()
        self.data = emission
        self.errors = errs
        
    def refresh(self):
        """
        Replacing '-' with 0 in the emission data.

        Returns
        -------
        None.

        """
        for key, value in self.data.items():
            for n, i in enumerate(value):
                if i == '-':
                    value[n] = 0
            # print(key, value)
        return
    
    def processing(self, R=None):
        """
        Process emission excel data for report.
        Mean over the time range.

        Parameters
        ----------
        R : list, optional
            List of upper and lower limit. The default is None.
            List = [startTime, endTime].

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        A = self.data.copy()
        del A['Date / time']
        del A['% Eff net']
        del A['% Eff gr.']
        del A['l/min Pump']
        del A['°C Tamb']
        del A['Dilution factor']
        del A['Fuel']
        # del A['°C Tstack']
        del A['ppm SO2']
        
        self.dataPlot = {key:value.astype(float) for key, value in A.items()}
        
        del A['sec Runtime']       
        if R == None:
            self.dataReport = {key:round(np.mean(value.astype(float)),2)
                               for key, value in A.items()}
            
            print("EMISSION : Range [R1,R2] is not given, complete average")
        else:
            self.dataReport = {key:round(np.mean
                               (value[R[0]:R[1]].astype(float)),2)
                               for key, value in A.items()}
        self.meanRange = R
        
        # ERROR ---------------------------------------------------------------    
        # reading random fractional error # not %
        re_E = np.array([self.errors.data['O2 (%)'][0]/100,
                         self.errors.data['CO (%)'][0]/100,
                         self.errors.data['NOx (%)'][0]/100,
                         self.errors.data['NO (%)'][0]/100,
                         self.errors.data['NO2 (%)'][0]/100,
                         self.errors.data['CO2 (%)'][0]/100,
                         self.errors.data['CxHy (%)'][0]/100])
        
        # reading systematic fractional error # not %
        sys_E = np.array([ef.testoErr(self.dataReport['% O2'], 'O2'),
                          ef.testoErr(self.dataReport['ppm CO'], 'CO'),
                          ef.testoErr(self.dataReport['ppm NOx'], 'NOx'),
                          ef.testoErr(self.dataReport['ppm NO'], 'NO'),
                          ef.testoErr(self.dataReport['ppm NO2'], 'NO2'),
                          ef.testoErr(self.dataReport['% CO2'], 'CO2'),
                          ef.testoErr(self.dataReport['ppm CxHy'], 'CxHy')])
        
        total_E = np.sqrt(np.square(re_E) + np.square(sys_E))
        total0_E = sys_E
        
        self.errorReport = {'% O2 Error': round(total_E[0]*self.dataReport['% O2'],2),
                             'ppm CO Error': round(total_E[1]*self.dataReport['ppm CO'],2),
                             'ppm NOx Error': round(total_E[2]*self.dataReport['ppm NOx'],2),
                             'ppm NO Error': round(total_E[3]*self.dataReport['ppm NO'],2),
                             'ppm NO2 Error': round(total_E[4]*self.dataReport['ppm NO2'],2),
                             '% CO2 Error': round(total_E[5]*self.dataReport['% CO2'],2),
                             'ppm CxHy Error': round(total_E[6]*self.dataReport['ppm CxHy'],2)}
        
        self.sysErrorReport = {'% O2 Error': round(total0_E[0]*self.dataReport['% O2'],2),
                             'ppm CO Error': round(total0_E[1]*self.dataReport['ppm CO'],2),
                             'ppm NOx Error': round(total0_E[2]*self.dataReport['ppm NOx'],2),
                             'ppm NO Error': round(total0_E[3]*self.dataReport['ppm NO'],2),
                             'ppm NO2 Error': round(total0_E[4]*self.dataReport['ppm NO2'],2),
                             '% CO2 Error': round(total0_E[5]*self.dataReport['% CO2'],2),
                             'ppm CxHy Error': round(total0_E[6]*self.dataReport['ppm CxHy'],2)}
        
        return self.dataReport # was not there while adding doc strings !
    
    def o2Report(self):
        """
        Emission data conversion to 15% O2 basis.
        Calling the Function O2Basis() from emissionO2basis.py module.

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        # Calculation of O2 Report without Errors
        er = self.dataReport
        data = [er['% O2'], er['ppm CO'], er['ppm NOx'], er['ppm NO'],\
                            er['ppm NO2'], er['% CO2'], er['ppm CxHy']]
        
        O2, CO, NOx, NO, NO2, CO2, CxHy = data[0], data[1], data[2], data[3],\
                                          data[4], data[5], data[6]
                                          
        self.o2DataReport = eob.O2Basis(O2, CO, NOx, NO, NO2, CO2, CxHy)
        
        # Calculation of O2 Report with Errors
        self.o2AllReport = eob.O2BasisWithError(O2, CO, NOx, NO, NO2, CO2, 
                                                CxHy, self.errorReport)
        # without random error
        self.o2AllSysReport = eob.O2BasisWithError(O2, CO, NOx, NO, NO2, CO2, 
                                                   CxHy, self.sysErrorReport)
        
        self.o2ErrorReport = self.o2AllReport[1]
        self.o2sysErrorReport = self.o2AllSysReport[1]
        
        print("\n>> Emission Analysis Report : 15% O2 Basis")
        print("Emission = ", self.o2AllReport)
        
        return self.o2DataReport # was not there while adding doc strings !
    
    def eiReport(self):
        """
        Emission data conversion to Emission Index (EI) format.
        Calling the Function EI() from emissionIndex.py module.

        Returns
        -------
        dict
            Result in dictionary format.
            dict = {'title' : np.array([values])}.

        """
        er = self.dataReport
        data = [er['% O2'], er['ppm CO'], er['ppm NOx'], er['ppm NO'],\
                            er['ppm NO2'], er['% CO2'], er['ppm CxHy']]
        
        CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet = data[1]/1e6, data[2]/1e6,\
                                                     data[3]/1e6, data[5]/1e2,\
                                                     data[6]/1e6 
                                                     
        # Calculation of EI without Errors
        self.eiDataReport = ei.EI(CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet)
        print("\n>> Emission Analysis Report : Emission Indices")
        print("Emission = ", self.eiDataReport)
        
        # Calculation of EI with Errors
        O2_wet = data[0]/1e2
        
        # with random and systematic error
        self.eiAllReport = ei.EIall(CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet, 
                                    O2_wet, self.errorReport)
        # with systematic error
        self.eiAllSysReport = ei.EIall(CO_wet, NOx_wet, NO_wet, CO2_wet, 
                                       CxHy_wet, O2_wet, self.sysErrorReport)
        
        self.eiErrorReport = self.eiAllReport
        self.eiSysErrorReport = self.eiAllSysReport
        return self.eiDataReport # was not there while adding doc strings !
    
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    
    