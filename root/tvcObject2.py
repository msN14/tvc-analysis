"""
*******************************************************************************
                | CLASS | CREATING TVC WORKING CONDITIONS
...............................................................................
DESCRIPTION:
    1.Fixing the variable and calculating control variables
    2.Properties classified into geometric, flow (or aerodynamic) and chemical
...............................................................................
REFERENCES:
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##---------------------------------------------[Imports]-----------------------
# import cantera as ct
import numpy as np
import gasProperties as gp
#------------------------------------------------------------------------------

##---------------------------------------------[Variables and Units]-----------
# geometric parameters [L, D, W, H, t]      [mm]
# pressure (P)                              [Pa]            
# temperature (T)                           [K]
# main velocity (Vx)                        [m/s]
# cavity velocity (Ux)                      [m/s]
# jet momentum flux ratio (J)               [-]
# equivalence ratio of cavity inlet (ERC)   [-]
# equivalence ratio of main inlet (ERO)     [-]
# overall equivalence ratio (ERO)           [-]
# mass flow rate ratio (MR)                 [-]
#------------------------------------------------------------------------------

def form1(H, P, T, V, J, ERC, ERM, DA):
    """
    Creating TVC state object in form1.
    form1 = [P, T, V, J, ERC, ERM, DA].

    Parameters
    ----------
    P : float
        Pressure of the combustor.
        Unit = [Pa]
    T : float
        Inlet temperature of the combustor.
        Unit = [K]
    V : float
        Main flow velocity.
        Unit = [m/s]
    J : float
        Jet momentum flux ratio (cavity/main).
        Unit = [-]
    ERC : float
        Equivalence ratio of cavity inlet.
        Unit = [-]
    ERM : float
        Equivalence ratio of main inlet.
        Unit = [-]
    DA : float
        Dilution air flow ratio (dilution/tvcFlow).
        Unit = [-]

    Returns
    -------
    state : object
        Object describing TVC state.

    """
    state = tvc(H=H, P=P,T=T,Vx=V,J=J,ERC=ERC,ERM=ERM,DA=DA)
    state.set_Y_density()
    state.set_U()
    state.set_mdot()
    state.set_mdot_dilution()
    state.set_ERO()
    state.set_power()
    
    return state

def form2(P, T, V, U, ERC, ERM, DA):
    """
    Creating TVC state object in form2.
    form2 = [P, T, V, U, ERC, ERM, DA].

    Parameters
    ----------
    P : float
        Pressure of the combustor.
        Unit = [Pa]
    T : float
        Inlet temperature of the combustor.
        Unit = [K]
    V : float
        Main flow velocity.
        Unit = [m/s]
    U : float
        Cavity flow velocity.
        Unit = [m/s]
    ERC : float
        Equivalence ratio of cavity inlet.
        Unit = [-]
    ERM : float
        Equivalence ratio of main inlet.
        Unit = [-]
    DA : float
        Dilution air flow ratio (dilution/tvcFlow).
        Unit = [-]

    Returns
    -------
    state : object
        Object describing TVC state.

    """
    state = tvc(P=P,T=T,Vx=V,Ux=U,ERC=ERC,ERM=ERM,DA=DA)
    state.set_Y_density()
    state.set_mdot()
    state.set_mdot_dilution()
    state.set_ERO()
    state.set_power()
    
    return state

def form3(P, T, V, ERC, ERM, ERO, DA):
    """
    Creating TVC state object in form3.
    form3 = [P, T, V, ERC, ERM, ERO, DA].

    Parameters
    ----------
    P : float
        Pressure of the combustor.
        Unit = [Pa]
    T : float
        Inlet temperature of the combustor.
        Unit = [K]
    V : float
        Main flow velocity.
        Unit = [m/s]
    ERC : float
        Equivalence ratio of cavity inlet.
        Unit = [-]
    ERM : float
        Equivalence ratio of main inlet.
        Unit = [-]
    ERO : float
        Overall equivalence ratios.
        Unit = [-]
    DA : float
        Dilution air flow ratio (dilution/tvcFlow).
        Unit = [-]

    Returns
    -------
    state : object
        Object describing TVC state.

    """
    state = tvc(P=P,T=T,Vx=V,ERC=ERC,ERM=ERM,ERO=ERO,DA=DA)
    state.set_Y_density()
    state.mdot_main = state.density_main*state.A_main*state.Vx
    state.mdot_main_fuel = state.mdot_main * state.YCH4_main
    state.mdot_main_air = state.mdot_main - state.mdot_main_fuel
    state.mdot_cavity_fuel = ((state.ERO/17.11)*state.mdot_main_air-
                              state.mdot_main_fuel)/(1-(state.ERO/state.ERC))
    state.mdot_cavity_air = (17.11*state.mdot_cavity_fuel)/state.ERC
    state.mdot_cavity = state.mdot_cavity_air + state.mdot_cavity_fuel
    state.Ux = state.mdot_cavity/(state.A_cavity*state.density_cavity)
    state.set_J()
    state.set_mdot_dilution()
    state.set_power()
    
    return state

def formReactor1(P, T, mdot_main, mdot_cavity, ERC, ERM):
    """
    Creating TVC state object in formReactor1.
    formReactor1 = [P, T, mdot_main, mdot_cavity, ERC, ERM].
    Used to specifiy reactor working conditions.

    Parameters
    ----------
    P : float
        Pressure of the combustor.
        Unit = [Pa]
    T : float
        Inlet temperature of the combustor.
        Unit = [K]
    mdot_main : float
        Mass flow rate of main flow.
        Unit = [kg/s]
    mdot_cavity : float
        Mass flow rate of cavity flow.
        Unit = [kg/s]
    ERC : float
        Equivalence ratio of cavity inlet.
        Unit = [-]
    ERM : float
        Equivalence ratio of main inlet.
        Unit = [-]

    Returns
    -------
    state : object
        Object describing TVC state.

    """
    state = tvc(P=P,T=T,mdot_main=mdot_main, mdot_cavity=mdot_cavity,
                ERC=ERC, ERM=ERM)
    state.set_Y_density()
    state.mdot_main_fuel = state.mdot_main * state.YCH4_main
    state.mdot_main_air = state.mdot_main - state.mdot_main_fuel
    state.mdot_cavity_fuel = state.mdot_cavity * state.YCH4_cavity
    state.mdot_cavity_air = state.mdot_cavity - state.mdot_cavity_fuel
    state.mdot_fuel = state.mdot_main_fuel + state.mdot_cavity_fuel
    state.mdot_air = state.mdot_main_air + state.mdot_cavity_air
    state.ERO = 17.11 * (state.mdot_fuel/state.mdot_air)
    state.Vx = state.mdot_main/(state.density_main*state.A_main)
    state.Ux = state.mdot_cavity/(state.density_cavity*state.A_cavity)
    state.set_power()
    
    return state

def formReactor2(P, T, mdot_cavity, ERC, ERM, ERO):
    """
    Creating TVC state object in formReactor2.
    formReactor1 = [P, T, mdot_cavity, ERC, ERM, ERO].
    Used to specifiy reactor working conditions.
    
    Parameters
    ----------
    P : float
        Pressure of the combustor.
        Unit = [Pa]
    T : float
        Inlet temperature of the combustor.
        Unit = [K]
    mdot_cavity : float
        Mass flow rate of cavity flow.
        Unit = [kg/s]
    ERC : float
        Equivalence ratio of cavity inlet.
        Unit = [-]
    ERM : float
        Equivalence ratio of main inlet.
        Unit = [-]
    ERO : float
        Overall equivalence ratios.
        Unit = [-]

    Returns
    -------
    state : object
        Object describing TVC state.

    """
    state = tvc(P = P,T = T, mdot_cavity = mdot_cavity,
                ERC = ERC, ERM = ERM, ERO = ERO)
    state.set_Y_density()
    state.mdot_cavity_fuel = state.mdot_cavity * state.YCH4_cavity
    state.mdot_cavity_air = state.mdot_cavity - state.mdot_cavity_fuel
    state.mdot_main_air = ((17.11*state.mdot_cavity_fuel) - \
                          (state.mdot_cavity_air*state.ERO))/ \
                          (state.ERO - state.ERM)
    state.mdot_main_fuel = (state.ERM/17.11)*state.mdot_main_air
    state.mdot_main = state.mdot_main_air + state.mdot_main_fuel
    state.Vx = state.mdot_main/(state.density_main*state.A_main)
    state.Ux = state.mdot_cavity/(state.density_cavity*state.A_cavity)
    state.set_power()
    
    return state

#:::::::::::::::::::::::::::::::::::::::::::::[Class Definitions]::::::::::::::
    
class tvc:
    
    def __init__(self, t=2, LD=1, L=30, D=30, H=20, W=50,
                  P=101325, T=300, Vx=None, Ux=None, J=None, 
                  mdot_main=None, mdot_cavity=None,
                  mdot_main_air=None, mdot_main_fuel=None,
                  mdot_cavity_air=None, mdot_cavity_fuel=None,
                  DA=1, mdot_dilution_air=None, density=None,
                  ERC=None, ERM=None, ERO=None, MR=None):
        """
        Initializing TVC object.
        Creating TVC object.

        Parameters
        ----------
        t : float, optional
            Cavity injection slot thickness. The default is 2.
            Unit = [mm]
        LD : float, optional
            L/D ratio of the cavity. The default is 1.
            Unit = [-]
        L : float, optional
            Cavity length. The default is 30.
            Unit = [mm]
        D : float, optional
            Cavity depth. The default is 30.
            Unit = [mm]
        H : float, optional
            Main duct height. The default is 20.
            Unit = [mm]
        W : float, optional
            Main duct width. The default is 50.
            Unit = [mm]
        P : float, optional
            Combustor pressure. The default is 101325.
            Unit = [Pa]
        T : float, optional
            Inlet temperature. The default is 300.
            Unit = [K]
        Vx : float, optional
            Main flow inlet velocity. The default is None.
            Unit = [m/s]
        Ux : float, optional
            Cavity flow inlet velocity. The default is None.
            Unit = [m/s]
        J : float, optional
            Jet momentum flux ratio (cavity/main). The default is None.
            Unit = [-]
        mdot_main : float, optional
            Mass flow rate of main flow. The default is None.
            Unit = [kg/s]
        mdot_cavity : float, optional
            Mass flow rate of cavity flow. The default is None.
            Unit = [kg/s]
        mdot_main_air : float, optional
            Mass flow rate of main air flow. The default is None.
            Unit = [kg/s]
        mdot_main_fuel : float, optional
            Mass flow rate of main fuel flow. The default is None.
            Unit = [kg/s]
        mdot_cavity_air : float, optional
            Mass flow rate of cavity air flow. The default is None.
            Unit = [kg/s]
        mdot_cavity_fuel : float, optional
            Mass flow rate of cavity fuel flow. The default is None.
            Unit = [kg/s]
        DA : float, optional
            Dilution air flow ratio (dilution/tvcFlow).. The default is 1.
            Unit = [-]
        mdot_dilution_air : float, optional
            Mass flow rate of dilution air flow. The default is None.
            Unit = [kg/s]
        density : float, optional
            Density of inlet flow. The default is None.
            Unit = [kg/m^3]
        ERC : float, optional
            Equivalence ratio of cavity inlet. The default is None.
            Unit = [-]
        ERM : float, optional
            Equivalence ratio of main inlet. The default is None.
            Unit = [-]
        ERO : float, optional
            Overall equivalence ratios. The default is None.
            Unit = [-]
        MR : float, optional
            Mass flow rate ratio (main/cavity). The default is None.
            Unit = [-]

        Returns
        -------
        None.

        """
        # Geometric variables
        self.t = t
        self.L = L
        self.D = D
        self.H = H
        self.W = W
        self.LD = self.L/self.D
        self.A_cavity = (self.t*self.W)*1e-6
        self.A_main = (self.H*self.W)*1e-6
        # Flow or aerodynamic variables
        self.P = P
        self.T = T
        self.Vx = Vx
        self.Ux = Ux
        self.J = J
        self.mdot_main = mdot_main
        self.mdot_cavity = mdot_cavity
        self.mdot_main_air = mdot_main_air
        self.mdot_main_fuel = mdot_main_fuel
        self.mdot_cavity_air = mdot_cavity_air
        self.mdot_cavity_fuel = mdot_cavity_fuel
        self.DA = DA
        self.mdot_dilution_air= mdot_dilution_air
        self.density = density
        # Chemical variables
        self.ERC = ERC
        self.ERM = ERM
        self.ERO = ERO
        self.MR = MR
        
        return
    
    def set_Y_density(self):
        """
        Calculating Yi and density using ERC and ERM.
        Cavity flow and main flow properties.
        
        Input = [ERC, ERM, P, T].

        Returns
        -------
        None.

        """
        A = gp.MethaneAir(self.ERC, self.P, self.T)
        self.YCH4_cavity = A[1][0]
        self.YO2_cavity = A[1][1]
        self.YN2_cavity = A[1][2]
        self.density_cavity = A[0]
        B = gp.MethaneAir(self.ERM, self.P, self.T)
        self.YCH4_main= B[1][0]
        self.YO2_main = B[1][1]
        self.YN2_main = B[1][2]
        self.density_main = B[0]
        
        return
    
    def set_U(self):
        """
        Calculate Ux from other variables.
        
        Input = [Vx, J, density_main, density_cavity].

        Returns
        -------
        None.

        """
        self.Ux = np.sqrt((self.J*self.density_main*(self.Vx**2))/
                          (self.density_cavity))
        return
    
    def set_mdot(self):
        """
        Calculate mdots of the flow.
        
        Input = [mdots] [density_main, density_cavity]
                [Vx, Ux] [A_main, A_caviy] [YCH4 YO2 YN2]
        Output = [mdots]

        Returns
        -------
        None.

        """
        self.mdot_main = self.density_main*self.A_main*self.Vx
        self.mdot_main_fuel = self.YCH4_main*self.mdot_main
        self.mdot_main_air = self.mdot_main - self.mdot_main_fuel
        self.mdot_cavity = self.density_cavity*self.A_cavity*self.Ux
        self.mdot_cavity_fuel = self.YCH4_cavity*self.mdot_cavity
        self.mdot_cavity_air = self.mdot_cavity - self.mdot_cavity_fuel
        self.mdot_fuel = self.mdot_main_fuel + self.mdot_cavity_fuel
        self.mdot_air = self.mdot_main_air + self.mdot_cavity_air
        
        return
    
    def set_mdot_dilution(self):
        """
        Calculate dilution air mass flow rate.
        
        Input = [mdot_main_air, DA]
        Output = [mdot_dilution_air]

        Returns
        -------
        None.

        """
        self.mdot_dilution_air = self.mdot_main_air * self.DA
        return
        
    def set_ERO(self):
        """
        Calculate ERO.
        
        Input = [mdot_fuel, mdot_air]
        Output = [ERO]

        Returns
        -------
        None.

        """
        self.ERO = 17.11 * (self.mdot_fuel/self.mdot_air)
        return
    
    def set_power(self):
        """
        Calculate power of combustor.
        
        Input = [mdot_main_fuel, mdot_cavity_fuel]
        Output = [power_cavity, power_main, power]

        Returns
        -------
        None.

        """
        self.power_cavity = self.mdot_cavity_fuel*50.016*1000
        self.power_main = self.mdot_main_fuel*50.016*1000
        self.power = self.power_cavity + self.power_main
        return
    
    def set_J(self):
        """
        Calculate J.
        
        Input = [density_cavity, density_main] [Ux, Vx]
        Output = [J]
                
        Returns
        -------
        None.

        """
        self.J = (self.density_cavity*(self.Ux**2)/
                 (self.density_main*(self.Vx**2)))
        return
    
    def set_MR(self):
        """
        Calculate MR.
        
        Input = [mdot_main, mdot_cavity]
        Output = [MR]

        Returns
        -------
        None.

        """
        self.MR = self.mdot_main/self.mdot_cavity
        return
    
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
