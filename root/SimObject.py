"""
*******************************************************************************
                        SIMULATION DATA TOOLS FOR TVC
...............................................................................
DESCRIPTION:
    1. Two types of data from simulations, line and plane data
    2. Process to plot line and plane data
    3. Plane data can be plotted as contour plots, streamlines and vector plots
...............................................................................
REFERENCES:
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
# import xlrd as xl
import numpy as np
import csv

import matplotlib.pyplot as plt
import matplotlib.tri as tri
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# geometric parameters [L, D, W, H, t]      [mm]
# pressure (P)                              [Pa]
# temperature (T)                           [K]
# main velocity (Vx)                        [m/s]
# cavity velocity (Ux)                      [m/s]
# jet momentum flux ratio (J)               [-]
# equivalence ratio of cavity inlet (ERC)   [-]
# equivalence ratio of main inlet (ERO)     [-]
# overall equivalence ratio (ERO)           [-]
#------------------------------------------------------------------------------


#:::::::::::::::::::::::::::::::::::::::::::::[Input/Output Pass]::::::::::::::

class IO:

    def LoadLineData(fileName):
        """
        Convert csv data to numpy array data

        Parameters
        ----------
        fileName : str
            Location of the line data.

        Returns
        -------
        simuData : dict
            Data in the format {'title':np.array([values])}.

        """
        with open(fileName) as csv_file:
            cs = csv.reader(csv_file, delimiter='\t')
            array = [row for row in cs if len(row)!=0 and row[0][0]!=
                     '(' and row[0][0]!=')']
            arrayT = np.transpose(array)
            simuData = {'y':arrayT[0].astype(float),
                        'variable':arrayT[1].astype(float)}
        return simuData

    def LoadPlaneData(fileName):
        """
        Convert csv data to numpy array data

        Parameters
        ----------
        fileName : str
            Location of the plane data.

        Returns
        -------
        dataDictionary : dict
            Data in the format {'title':np.array([values])}.

        """
        with open(fileName) as csv_file:
            cs = csv.reader(csv_file, delimiter=',')
            array = [row for row in cs]
            arrayT=np.transpose(array)
            dataDictionary={x[0].strip(): np.array(x[1:].astype(float))
                            for x in arrayT}
        return dataDictionary

    def createSimObject(fileName):
        """
        Creating simulation object based on the loaded files.
        * now only plane data loaded.

        Parameters
        ----------
        fileName : str
            Location of the data.

        Returns
        -------
        object
            Simulation object with line and plane data.
            * now only plane data loaded

        """
        p=Plane(IO.LoadPlaneData(fileName))
        
        return Simulation("",p)

#:::::::::::::::::::::::::::::::::::::::::::::[Simulation Object]::::::::::::::

class Simulation:

    def __init__(self, line, plane):
        """
        Initializing simulation object.

        Parameters
        ----------
        line : object
            Object to pass line data.
        plane : object
            Object to pass plane data.

        Returns
        -------
        None.

        """
        self.line = line
        self.plane = plane
        
        return

#:::::::::::::::::::::::::::::::::::::::::::::[Line Data Object]:::::::::::::::

class Line:

    def __init__(self, lineData):
        """
        Initializing line object.

        Parameters
        ----------
        lineData : dict
            Dictionary data from loaded line data.

        Returns
        -------
        None.

        """
        # self.title=conditions.keys()
        # self.values=conditions.values()
        self.lineData = lineData

    def linePlot(self,rowNumber=None):
        """
        Plotting line data.

        Parameters
        ----------
        rowNumber : int, optional
            Specifying the row. The default is None.

        Returns
        -------
        None.

        """
        if rowNumber == None:
            rowNumber = 1
            print("\nRow number not given !")
        self.dataReport={key:value[rowNumber]
                         for key, value in self.data.items()}
        del self.dataReport['Time']
        return(self.dataReport)

    def advancedPlot(dicData):
        return

#:::::::::::::::::::::::::::::::::::::::::::::[Plane Data Object]::::::::::::::

class Plane:

    def __init__(self, planeData):
        """
        Initializing plane object.

        Parameters
        ----------
        planeData : dict
            Dictionary data from loaded plane data..

        Returns
        -------
        None.

        """
        # self.title=conditions.keys()
        # self.values=conditions.values()
        self.planeData = planeData

    def contourPlot(self, figNum, variableKeyName, outputName):
        """
        Plotting contours.

        Parameters
        ----------
        figNum : int
            Figure number.
        variableKeyName : str
            Name of the variable (key) in the data dictionary.
        outputName : str
            Name of the plot file.

        Returns
        -------
        None.

        """
        x = self.planeData['x-coordinate']*1000
        y = self.planeData['y-coordinate']*1000
        v = self.planeData[variableKeyName]

        triang = tri.Triangulation(x, y) #Triagulation

        # Mask off unwanted triangles.
        xmid = x[triang.triangles].mean(axis=1)
        ymid = y[triang.triangles].mean(axis=1)
        mask1 = np.where(xmid <=0,ymid <=-10, 0)
        mask2 = np.where(xmid >=30,ymid <=-10, 0)
        triang.set_mask(mask1+mask2)

        fig1, ax1 = plt.subplots(num=figNum,dpi=200,figsize=(11.5/1.5,4/1.5))
        ax1.set_aspect('auto')
        tcf = ax1.tricontourf(triang, v, 30)
        plt.axis('scaled')

        bbox = dict(boxstyle="square", fc="1")
        arrowprops = dict(arrowstyle = "->",
                  connectionstyle = "angle,angleA=90,angleB=180,rad=0")
        ax1.annotate('Cavity Flow', xy=(0,-39),  xycoords='data',
             xytext=(-25, -28),size=8,bbox=bbox,arrowprops=arrowprops)

        axins = inset_axes(ax1,
           width="2%",  # width = 5% of parent_bbox width
           height="100%",  # height : 50%
           loc='lower left',
           bbox_to_anchor=(1.01, 0., 1, 1),
           bbox_transform=ax1.transAxes,
           borderpad=0,
           )

        fig1.colorbar(tcf,cax=axins)
        # fig1.colorbar(tcf, ax=ax1)
        ax1.set_title('Velocity Contour Plot')
        fig1.savefig('./'+outputName+'.pdf', dpi=300)
        
        return

    def streamlinePlot(self, Ux, Uy):
        """
        Plotting streamlines.

        Parameters
        ----------
        Ux : numpy.ndarray
            velocity in x direction.
        Uy : numpy.ndarray
            velocity in y direction.

        Returns
        -------
        None.

        """
        x = self.planeData['x-coordinate']*1000
        y = self.planeData['y-coordinate']*1000
        u = self.planeData[Ux]
        v = self.planeData[Uy]

        ngridx = 10*175
        ngridy = 10*50

        # xi = np.linspace(-0.03, 0.145, ngridx)
        # yi = np.linspace(-0.04, 0.01, ngridy)
        start = [[0,-38], [0,-39], [0,-40], [0,0], [-20,5], [-20,10],
                 [-20,2], [-20,4], [-20,6], [-20,8], [-20,-2], [-20,-4], 
                 [-20,-6], [-20,-8]]

        xu = np.linspace(-30, 145, ngridx)
        yu = np.linspace(-40, 10, ngridy)

        uu = griddata((x, y), u, (xu[None,:], yu[:,None]), method='nearest')
        vu = griddata((x, y), v, (xu[None,:], yu[:,None]), method='nearest')

        fig1, ax1 = plt.subplots(num=2,dpi=200,figsize=(11.5/1.5,4/1.5))
        ax1.set_aspect('auto')

        ax1.streamplot(xu, yu, uu, vu, density = 2.5, color='k',
                       linewidth = 0.5, maxlength = 1, 
                       start_points = start , arrowsize = 1)

        fig1.savefig('./streamline.png')
        return

    def streamContourPlot(self, Ux, Uy, variableKeyName, figNum):
        """
        Plotting streamlines on contour plot.

        Parameters
        ----------
        Ux : numpy.ndarray
            velocity in x direction.
        Uy : numpy.ndarray
            velocity in y direction.
        variableKeyName : str
            Name of the variable (key) in the data dictionary.
        figNum : int
            Figure number.

        Returns
        -------
        None.

        """
        x = self.planeData['x-coordinate']*1000
        y = self.planeData['y-coordinate']*1000
        Umag = self.planeData[variableKeyName]

        triang = tri.Triangulation(x, y) #Triagulation
        # Mask off unwanted triangles.
        xmid = x[triang.triangles].mean(axis=1)
        ymid = y[triang.triangles].mean(axis=1)
        mask1 = np.where(xmid <=0,ymid <=-10, 0)
        mask2 = np.where(xmid >=30,ymid <=-10, 0)
        triang.set_mask(mask1+mask2)

        u = self.planeData[Ux]
        v = self.planeData[Uy]
        ngridx = 10*175
        ngridy = 10*50

        #Starting the streamlines
        start = [[0,-38], [0,-39], [0,-40], [0,0],
         [-20,2], [-20,4], [-20,6], [-20,8], [-20,-2], [-20,-4], [-20,-6], 
         [-20,-8],
         [40,2], [40,4], [40,6], [40,8], [40,-2], [40,-4], [40,-6], [40,-8],
         [100,2], [100,4], [100,6], [100,8], [100,-2], [100,-4], [100,-6], 
         [100,-8],
         [2,-12], [2,-20], [2,-30],
         [5,-10], [5,-20], [5,-30],
         [10,-10], [10,-20], [10,-30],
         [20,-10], [20,-20], [20,-30]]

        xu = np.linspace(-30, 145, ngridx)
        yu = np.linspace(-40, 10, ngridy)
        uu = griddata((x, y), u, (xu[None,:], yu[:,None]), method='nearest')
        vu = griddata((x, y), v, (xu[None,:], yu[:,None]), method='nearest')


        fig1, ax1 = plt.subplots(num=figNum,dpi=200,figsize=(11.5/1.5,4/1.5))
        ax1.set_aspect('auto')
        tcf = ax1.tricontourf(triang, Umag, 30)
        ax1.streamplot(xu, yu, uu, vu, density = 2.5, color='w', 
                       linewidth = 0.5, maxlength = 1, 
                       start_points = start , arrowsize = 1)

        plt.axis('scaled')
        bbox = dict(boxstyle="square", fc="1")
        arrowprops = dict(arrowstyle = "->",
                  connectionstyle = "angle,angleA=90,angleB=180,rad=0")
        ax1.annotate('Cavity Flow', xy=(0,-39),  xycoords='data',
             xytext=(-25, -28),size=6,bbox=bbox,arrowprops=arrowprops)

        axins = inset_axes(ax1,
                   width="2%",  # width = 5% of parent_bbox width
                   height="100%",  # height : 50%
                   loc='lower left',
                   bbox_to_anchor=(1.01, 0., 1, 1),
                   bbox_transform=ax1.transAxes,
                   borderpad=0,
                   )

        fig1.colorbar(tcf,cax=axins)
        ax1.set_title('Streamlines and '+variableKeyName+' contour')

        fig1.savefig('./'+'strem_'+variableKeyName+'_'+str(figNum)+'.png')

        return

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
