import matplotlib.pyplot as plt
import numpy as np

def tempTimePlot(ID, tempDict, report, R, display):
    """
    Plotting Time-Temperature with averaged quantites and ranges.

    Parameters
    ----------
    ID : str
        Condition ID of the experiment.
        Example : 'P2T300LD1.0t2V10J3.4ERC2.0ERM0.2DA1.0'
    tempDict : dict
        Temperature data in dictionary format.
    report : dict
        Averaged data with specified time range.
    R : list
        List of upper and lower limit.
        List = [startTime, endTime].

    Returns
    -------
    None.

    """
    fig, (ax) = plt.subplots(4, 2, figsize = (10,8))
    fig.subplots_adjust(hspace=0.6)
    
    time = tempDict['Time']
    T1 = tempDict['T1 (K)']
    T2 = tempDict['T2 (K)']
    T3 = tempDict['T3 (K)']
    T4 = tempDict['T4 (K)']
    T5 = tempDict['T5 (K)']
    T6 = tempDict['T6 (K)']
    T7 = tempDict['T7 (K)']
    T8 = tempDict['T8 (K)']
    
    # Mapping from actual time range to data point location
    meanRange = [np.where(time < R[0])[0][-1], np.where(time < R[1])[0][-1]]
    
    ax[0,0].plot(time, T1, 'k', linewidth=0.6)
    ax[0,0].set_title('T1 Main')
    ax[0,0].axvline(x = time[meanRange[0]], color='r')
    ax[0,0].axvline(x = time[meanRange[1]], color='r')
    ax[0,0].text(1, 1.05, 'T1 = '+str(report['T1 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[0,0].transAxes,
                 color='black', fontsize=10)
    
    ax[0,1].plot(time, T2, 'k', linewidth=0.6)
    ax[0,1].set_title('T2 Cavity')
    ax[0,1].axvline(x = time[meanRange[0]], color='r')
    ax[0,1].axvline(x = time[meanRange[1]], color='r')
    ax[0,1].text(1, 1.05, 'T2 = '+str(report['T2 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[0,1].transAxes,
                 color='black', fontsize=10)
    
    ax[1,0].plot(time, T3, 'k', linewidth=0.6)
    ax[1,0].set_title('T3 Combustor Wall')
    ax[1,0].axvline(x = time[meanRange[0]], color='r')
    ax[1,0].axvline(x = time[meanRange[1]], color='r')
    ax[1,0].text(1, 1.05, 'T3 = '+str(report['T3 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[1,0].transAxes,
                 color='black', fontsize=10)
    
    ax[1,1].plot(time, T4, 'k', linewidth=0.6)
    ax[1,1].set_title('T4 Flame (135,0)')
    ax[1,1].axvline(x = time[meanRange[0]], color='r')
    ax[1,1].axvline(x = time[meanRange[1]], color='r')
    ax[1,1].text(1, 1.05, 'T4 = '+str(report['T4 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[1,1].transAxes,
                 color='black', fontsize=10)
    
    ax[2,0].plot(time, T5, 'k', linewidth=0.6)
    ax[2,0].set_title('T5 Flame (230,-5)')
    ax[2,0].axvline(x = time[meanRange[0]], color='r')
    ax[2,0].axvline(x = time[meanRange[1]], color='r')
    ax[2,0].text(1, 1.05, 'T5 = '+str(report['T5 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[2,0].transAxes,
                 color='black', fontsize=10)
    
    ax[2,1].plot(time, T6, 'k', linewidth=0.6)
    ax[2,1].set_title('T6 Flame (230,0)')
    ax[2,1].axvline(x = time[meanRange[0]], color='r')
    ax[2,1].axvline(x = time[meanRange[1]], color='r')
    ax[2,1].text(1, 1.05, 'T6 = '+str(report['T6 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[2,1].transAxes,
                 color='black', fontsize=10)
    
    ax[3,0].plot(time, T7, 'k', linewidth=0.6)
    ax[3,0].set_title('T7 Flame (230,5)')
    ax[3,0].axvline(x = time[meanRange[0]], color='r')
    ax[3,0].axvline(x = time[meanRange[1]], color='r')
    ax[3,0].text(1, 1.05, 'T7 = '+str(report['T7 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[3,0].transAxes,
                 color='black', fontsize=10)
    
    ax[3,1].plot(time, T8, 'k', linewidth=0.6)
    ax[3,1].set_title('T8 Exit')
    ax[3,1].axvline(x = time[meanRange[0]], color='r')
    ax[3,1].axvline(x = time[meanRange[1]], color='r')
    ax[3,1].text(1, 1.05, 'T8 = '+str(report['T8 (K)'])+' K',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[3,1].transAxes,
                 color='black', fontsize=10)
    
    for i in [0,1,2,3]:
        for j in [0,1]:
            ax[i,j].set_xlabel('Time [s]')
            ax[i,j].set_ylabel('Temperature [K]')
            ax[i,j].grid(True)
            
    fig.tight_layout()  
    
    # display plot or not
    if display =='no':
        plt.ioff()
    else:
        plt.ion()
    fig.savefig(ID+'/temperature-time.png', dpi=300)
    
    return