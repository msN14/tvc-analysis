import matplotlib.pyplot as plt

def mfcTimePlot(ID, mfcDict, report, meanRange, display):
    """
    Plotting Time-MFC with averaged quantites and ranges.

    Parameters
    ----------
    ID : str
        Condition ID of the experiment.
        Example : 'P2T300LD1.0t2V10J3.4ERC2.0ERM0.2DA1.0'
    mfcDict : dict
        MFC data in dictionary format.
    report : dict
        Averaged data with specified time range.
    meanRange : list
        List of upper and lower limit.
        List = [startTime, endTime].

    Returns
    -------
    None.

    """
    # display plot or not
    if display =='no':
        plt.ioff()
    else:
        plt.ion() 
        
    fig, (ax) = plt.subplots(3, 2, figsize = (10,8))
    fig.subplots_adjust(hspace=1)
    
    time = mfcDict['Time']
    MA1 = mfcDict['Main Air 1 (slpm)']
    MA2 = mfcDict['Main Air 2 (slpm)']
    MF = mfcDict['Main Fuel (slpm)']
    CA = mfcDict['Cavity Air (slpm)']
    CF = mfcDict['Cavity Fuel (slpm)']
    DA = mfcDict['Dilution Air (slpm)']
    
    MA1set = mfcDict['Main Air 1 (set)']
    MA2set = mfcDict['Main Air 2 (set)']
    MFset = mfcDict['Main Fuel (set)']
    CAset = mfcDict['Cavity Air (set)']
    CFset = mfcDict['Cavity Fuel (set)']
    DAset = mfcDict['Dilution Air (set)']
    
    
    ax[0,0].plot(time, MA1, 'k', linewidth=1.0, label='Measured')
    ax[0,0].plot(time, MA1set, 'k--', linewidth=1.0, label='Set')
    ax[0,0].set_title('Main Air 1')
    ax[0,0].legend()
    ax[0,0].text(1, 1.025, 'MA1 = '+str(report['Main Air 1 (slpm)'])+' slpm',
             verticalalignment='bottom', horizontalalignment='right',
             transform=ax[0,0].transAxes,
             color='black', fontsize=10)
        
    ax[0,1].plot(time, MA2, 'k', linewidth=1.0, label='Measured')
    ax[0,1].plot(time, MA2set, 'k--', linewidth=1.0, label='Set')
    ax[0,1].set_title('Main Air 2')
    ax[0,1].legend()
    ax[0,1].text(1, 1.025, 'MA2 = '+str(report['Main Air 2 (slpm)'])+' slpm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[0,1].transAxes,
         color='black', fontsize=10)
    
    ax[1,0].plot(time, MF, 'k', linewidth=1.0, label='Measured')
    ax[1,0].plot(time, MFset, 'k--', linewidth=1.0, label='Set')
    ax[1,0].set_title('Main Fuel')
    ax[1,0].legend()
    ax[1,0].text(1, 1.025, 'MF = '+str(report['Main Fuel (slpm)'])+' slpm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[1,0].transAxes,
         color='black', fontsize=10)
    
    ax[1,1].plot(time, CA, 'k', linewidth=1.0, label='Measured')
    ax[1,1].plot(time, CAset, 'k--', linewidth=1.0, label='Set')
    ax[1,1].set_title('Cavity Air')
    ax[1,1].legend()
    ax[1,1].text(1, 1.025, 'CA = '+str(report['Cavity Air (slpm)'])+' slpm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[1,1].transAxes,
         color='black', fontsize=10)
    
    ax[2,0].plot(time, CF, 'k', linewidth=1.0, label='Measured')
    ax[2,0].plot(time, CFset, 'k--', linewidth=1.0, label='Set')
    ax[2,0].set_title('Cavity Fuel')
    ax[2,0].legend()
    ax[2,0].text(1, 1.025, 'CF = '+str(report['Cavity Fuel (slpm)'])+' slpm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[2,0].transAxes,
         color='black', fontsize=10)
    
    ax[2,1].plot(time, DA, 'k', linewidth=1.0, label='Measured')
    ax[2,1].plot(time, DAset, 'k--', linewidth=1.0, label='Set')
    ax[2,1].set_title('Dilution Air')
    ax[2,1].legend()
    ax[2,1].text(1, 1.025, 'DA = '+str(report['Dilution Air (slpm)'])+' slpm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[2,1].transAxes,
         color='black', fontsize=10)
    
    for i in [0,1,2]:
        for j in [0,1]:
            ax[i,j].set_xlabel('Time [s]')
            ax[i,j].set_ylabel('SLPM')
            ax[i,j].axvline(x = time[meanRange[0]], color='r')
            ax[i,j].axvline(x = time[meanRange[1]], color='r')
            ax[i,j].grid(True)
                  
    fig.tight_layout()  
    
    # display plot or not
    if display =='no':
        plt.ioff()
    else:
        plt.ion() 
        
    fig.savefig(ID+'/mfc-time.png', dpi=300)
    
    return