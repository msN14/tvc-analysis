import matplotlib.pyplot as plt
import numpy as np

def pressureTimePlot(ID, pressureDict, report, R, display):
    """
    Plotting Time-Pressure with averaged quantites and ranges.

    Parameters
    ----------
    ID : str
        Condition ID of the experiment.
        Example : 'P2T300LD1.0t2V10J3.4ERC2.0ERM0.2DA1.0'
    pressureDict : dict
        Pressure data in dictionary format.
    report : dict
        Averaged data with specified time range.
    R : list
        List of upper and lower limit.
        List = [startTime, endTime].

    Returns
    -------
    None.

    """
    fig, (ax) = plt.subplots(4, 2, figsize = (10,8))
    fig.subplots_adjust(hspace=0.6)
    
    time = pressureDict['Time']
    P1 = pressureDict['P1 (bar)']
    P2 = pressureDict['P2 (bar)']
    P3 = pressureDict['P3 (bar)']
    P4 = pressureDict['P4 (bar)']
    P5 = pressureDict['P5 (bar)']
    P6 = pressureDict['P6 (bar)']
    P7 = pressureDict['P7 (bar)']
    
    meanRange = [np.where(time < R[0])[0][-1], np.where(time < R[1])[0][-1]]
    
    ax[0,0].plot(time, P1, 'k', linewidth=0.6)
    ax[0,0].set_title('P1 Air Supply')
    ax[0,0].text(1, 1.05, 'P1 = '+str(report['P1 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[0,0].transAxes,
                 color='black', fontsize=10)
    ax[0,1].plot(time, P2, 'k', linewidth=0.6)
    ax[0,1].set_title('P2 Fuel Supply')
    ax[0,1].text(1, 1.05, 'P2 = '+str(report['P2 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[0,1].transAxes,
                 color='black', fontsize=10)
    
    ax[1,0].plot(time, P3, 'k', linewidth=0.6)
    ax[1,0].set_title('P3 Main Air '+r'$\rightarrow$'+' Nozzle')
    ax[1,0].text(1, 1.05, 'P3 = '+str(report['P3 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[1,0].transAxes,
                 color='black', fontsize=10)
    ax[1,1].plot(time, P4, 'k', linewidth=0.6)
    ax[1,1].set_title('P4 Main Fuel '+r'$\rightarrow$'+' Nozzle')
    ax[1,1].text(1, 1.05, 'P4 = '+str(report['P4 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[1,1].transAxes,
                 color='black', fontsize=10)
    
    ax[2,0].plot(time, P5, 'k', linewidth=0.6)
    ax[2,0].set_title('P5 Cavity A/F '+r'$\rightarrow$'+' Nozzle')
    ax[2,0].text(1, 1.05, 'P5 = '+str(report['P5 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[2,0].transAxes,
                 color='black', fontsize=10)
    ax[2,1].plot(time, P6, 'k', linewidth=0.6)
    ax[2,1].set_title('P6 Dilution Air '+r'$\rightarrow$'+' Nozzle')
    ax[2,1].text(1, 1.05, 'P6 = '+str(report['P6 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[2,1].transAxes,
                 color='black', fontsize=10)
    
    ax[3,0].plot(time, P7, 'k', linewidth=0.6)
    ax[3,0].set_title('P7 Combustor')
    ax[3,0].text(1, 1.05, 'P7 = '+str(report['P7 (bar)'])+' bar',
                 verticalalignment='bottom', horizontalalignment='right',
                 transform=ax[3,0].transAxes,
                 color='black', fontsize=10)
    
    for i in [0, 1, 2, 3]:
        for j in [0, 1]:
            ax[i,j].set_xlabel('Time [s]')
            ax[i,j].set_ylabel('Pressure [bar]')
            ax[i,j].axvline(x = time[meanRange[0]], color='r')
            ax[i,j].axvline(x = time[meanRange[1]], color='r')
            ax[i,j].grid(True)
    
    fig.delaxes(ax[3,1]) 
    fig.tight_layout()   
    
    # display plot or not    
    if display =='no':
        plt.ioff()
    else:
        plt.ion()
    fig.savefig(ID+'/pressure-time.png', dpi=300)
    
    return