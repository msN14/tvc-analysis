#%% SECTION 1
#----------------------------------------------[Imports]-----------------------
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np
import os
#------------------------------------------------------------------------------

# -- cmap lists
# cmap = 'gist_heat' 'gist_heat' 'RdYlGn' RdBu

# plot_color_gradients('Diverging',
#                      ['PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu', 'RdYlBu',
#                       'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic'])

#----------------------------------------------[Plot Style]--------------------
plt.style.use('contour-style') # Peoper Formatting
#------------------------------------------------------------------------------

#---------------------------------------------[Plot 01 ]-----------------------

def contour_plotter(num, name, data, clevel, cmap, ctickf, spath):
    
    # create the save path
    if os.path.isdir(spath) == False:
        os.mkdir(spath)
    
    # create figure and axis
    fig, ax = plt.subplots(num)
    ax[0].invert_yaxis() # invert axis
    
    # Masking the cavity wall region
    data[300:,450:] = np.nan # error if data is interger, convert data to float
    
    dx = 900 - 50
    dy = 950 - 200
    
    # -- coordinate (x,y)
    x = np.arange(0,dx)
    y = np.arange(0,dy)
    
    # -- plotting using contourf
    tcf = ax[0].contourf(x,y, data, clevel, cmap = cmap)
    
    # -- plotting using imshow
    # ax[0].imshow(p[500], cmap='viridis', origin='lower')
    
    # -- removing white intersection lines
    for c in tcf.collections:
        c.set_edgecolor("face")
        
    # -- color bar configuration
    axins = inset_axes(ax[0],
        width="4%",  # width = 5% of parent_bbox width
        height="100%",  # height : 50%
        loc='lower left',
        bbox_to_anchor=(1.05, 0., 1, 1), #1.02
        bbox_transform=ax[0].transAxes,
        borderpad=0
        )
    
    # bar formates = '% 1.1f' '%.2e'
    bar_value_format = ctickf        # '% 1.4f' # '%.2e'
    ticks = None
    cbar = fig.colorbar(tcf, cax=axins, format=bar_value_format, ticks=ticks)
    # cbar.ax[0].yaxis.set_tick_params(pad=2)
    cbar.ax[0].tick_params(axis='both', which='minor', left=False, right=False)
    
    # -- scale the figure
    # ax[0].axis('scaled')
    ax[0].set_aspect('equal', adjustable='box', anchor='C')
    
    # ax[0].axhline(y=60, color='w', linestyle='dashed', linewidth=0.5)
    
    # -- ticks control
    ax[0].tick_params(axis='both', which='minor', left=False, right=False)
    ax[0].tick_params(axis='both', which='minor', top=False, bottom=False)
    ax[0].tick_params(axis='both', which='major', top=False, right=False)
    
    ax[0].set_xticks([0,150,300,450,600,750])
    ax[0].set_yticks([0,150,300,450,600,750])
    ax[0].set_xticklabels([0,10,20,30,40,50])
    ax[0].set_yticklabels([-10,0,10,20,30,40])
    
    # plotting cavity walls
    ax[0].plot([450,450,850],[750,300,300],'k', linewidth=0.8)
    ax[0].set_xlim([0,850])
    ax[0].set_ylim([750,0])
    
    # -- save the figure
    fig.savefig(spath+name+'.pdf')
    fig.savefig(spath+name+'.svg')
    fig.savefig(spath+name+'.png')
    
#------------------------------------------------------------------------------
