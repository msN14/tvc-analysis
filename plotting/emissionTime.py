import matplotlib.pyplot as plt

def emissionTimePlot(ID, emissionDict, report, meanRange, display):
    """
    Plotting Time-Emission with averaged quantites and ranges.

    Parameters
    ----------
    ID : str
        Condition ID of the experiment.
        Example : 'P2T300LD1.0t2V10J3.4ERC2.0ERM0.2DA1.0'
    emissionDict : dict
        Emission data in dictionary format.
    report : dict
        Averaged data with specified time range.
    meanRange : list
        List of upper and lower limit.
        List = [startTime, endTime].

    Returns
    -------
    None.

    """
    
    fig, (ax) = plt.subplots(4, 2, figsize = (10,8))
    fig.subplots_adjust(hspace=0.6)
    
    time = emissionDict['sec Runtime']
    O2 = emissionDict['% O2']
    CO2 = emissionDict['% CO2']
    CO = emissionDict['ppm CO']
    CxHy = emissionDict['ppm CxHy']
    NOx = emissionDict['ppm NOx']
    NO = emissionDict['ppm NO']
    NO2 = emissionDict['ppm NO2']
    
    ax[0,0].plot(time, O2, 'k', linewidth=1.0)
    ax[0,0].set_title('O2')
    ax[0,0].set_xlabel('Time [s]')
    ax[0,0].set_ylabel('O2 [%]')
    ax[0,0].axvline(x = time[meanRange[0]], color='r')
    ax[0,0].axvline(x = time[meanRange[1]], color='r')
    # plt.xticks(rotation=45)
    ax[0,0].grid(True)
    ax[0,0].text(1, 1.05, 'O2 = '+str(report['% O2'])+' %',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[0,0].transAxes,
         color='black', fontsize=10)
    
    ax[1,0].plot(time, CO2, 'k', linewidth=1.0)
    ax[1,0].set_title('CO2')
    ax[1,0].set_xlabel('Time [s]')
    ax[1,0].set_ylabel('CO2 [%]')
    ax[1,0].axvline(x = time[meanRange[0]], color='r')
    ax[1,0].axvline(x = time[meanRange[1]], color='r')
    # plt.xticks(rotation=45)
    ax[1,0].grid(True)
    ax[1,0].text(1, 1.05, 'CO2 = '+str(report['% CO2'])+' %',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[1,0].transAxes,
         color='black', fontsize=10)
    
    ax[2,0].plot(time, CO, 'k', linewidth=1.0)
    ax[2,0].set_title('CO')
    ax[2,0].set_xlabel('Time [s]')
    ax[2,0].set_ylabel('CO [ppm]')
    ax[2,0].axvline(x = time[meanRange[0]], color='r')
    ax[2,0].axvline(x = time[meanRange[1]], color='r')
    # plt.xticks(rotation=45)
    ax[2,0].grid(True)
    ax[2,0].text(1, 1.05, 'CO = '+str(report['ppm CO'])+' ppm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[2,0].transAxes,
         color='black', fontsize=10)
    
    ax[3,0].plot(time, CxHy, 'k', linewidth=1.0)
    ax[3,0].set_title('CxHy')
    ax[3,0].set_xlabel('Time [s]')
    ax[3,0].set_ylabel('CxHy [ppmC]')
    ax[3,0].axvline(x = time[meanRange[0]], color='r')
    ax[3,0].axvline(x = time[meanRange[1]], color='r')
    ax[3,0].grid(True)
    ax[3,0].text(1, 1.05, 'CxHy = '+str(report['ppm CxHy'])+' ppmC',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[3,0].transAxes,
         color='black', fontsize=10)
    
    ax[0,1].plot(time, NOx, 'k', linewidth=1.0)
    ax[0,1].set_title('NOx')
    ax[0,1].set_xlabel('Time [s]')
    ax[0,1].set_ylabel('NOx [ppm]')
    ax[0,1].axvline(x = time[meanRange[0]], color='r')
    ax[0,1].axvline(x = time[meanRange[1]], color='r')
    ax[0,1].grid(True)
    ax[0,1].text(1, 1.05, 'NOx = '+str(report['ppm NOx'])+' ppm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[0,1].transAxes,
         color='black', fontsize=10)
    
    ax[1,1].plot(time, NO, 'k', linewidth=1.0)
    ax[1,1].set_title('NO')
    ax[1,1].set_xlabel('Time [s]')
    ax[1,1].set_ylabel('NO [ppm]')
    ax[1,1].axvline(x = time[meanRange[0]], color='r')
    ax[1,1].axvline(x = time[meanRange[1]], color='r')
    ax[1,1].grid(True)
    ax[1,1].text(1, 1.05, 'NO = '+str(report['ppm NO'])+' ppm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[1,1].transAxes,
         color='black', fontsize=10)
    
    ax[2,1].plot(time, NO2, 'k', linewidth=1.0)
    ax[2,1].set_title('NO2')
    ax[2,1].set_xlabel('Time [s]')
    ax[2,1].set_ylabel('NO2 [ppm]')
    ax[2,1].axvline(x = time[meanRange[0]], color='r')
    ax[2,1].axvline(x = time[meanRange[1]], color='r')
    ax[2,1].grid(True)
    ax[2,1].text(1, 1.05, 'NO2 = '+str(report['ppm NO2'])+' ppm',
         verticalalignment='bottom', horizontalalignment='right',
         transform=ax[2,1].transAxes,
         color='black', fontsize=10)

    fig.delaxes(ax[3,1]) 
    
    # display plot or not
    if display =='no':
        plt.ioff()
    else:
        plt.ion()
    
    fig.tight_layout()    
    fig.savefig(ID+'/emission-time.png', dpi=300)
    
    return
    