"""
*******************************************************************************
                  EMISSION ANALYSIS (REPRESENT IN 15% O2)
...............................................................................
DESCRIPTION:
    1) Function input = CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet in X(i)
       Mole fraction Entry (X(i))
       
    2) Output is Emission in 15% O2

    CmHnOpNqSr + X[R(O2)+S(N2)+T(CO2)+h(H2O)+U(CH4)] -->
        P1(CO2+P2(N2)+P3(O2)+P4(H2O)+P5(CO)+P6(CxHy)+P7(NO2)+P8(NO)+P9(SO2))
...............................................................................
REFERENCES:
    1. South Coast Air Quality Management District :
       Source Test Protocol For Determining Oxygen Corrected Pollutant 
       Concentrations From Combustion Sources With High Stack Oxygen 
       Content Based On Carbon Dioxide Emissions
...............................................................................
DEVELOPER:  NISANTH M. S | m.nisanth.s@gmail.com  | Date: 12-06-2020
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
# import cantera as ct
import numpy as np
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# pressure         [Pa]            
# temperature      [K]
# equi_ratio       [-]
#------------------------------------------------------------------------------

def O2Basis(O2, CO, NOx, NO, NO2, CO2, CxHy):
    """
    Emission reading at 15% O2 basis.

    Parameters
    ----------
    O2 : float
        Measured O2.
        Unit = [%]
    CO : float
        Measured CO.
        Unit = [ppm]
    NOx : float
        Measured NOx.
        Unit = [ppm]
    NO : float
        Measured NO.
        Unit = [ppm]
    NO2 : float
        Measured NO2.
        Unit = [ppm]
    CO2 : float
        Measured CO2.
        Unit = [%]
    CxHy : float
        Measured CxHy in C1 basis.
        Unit = [ppmC]

    Returns
    -------
    emiDict : dict
        Emission data at 15% O2.
        {CO, NOx, NO, NO2, CO2, CxHy} in 15% O2

    """
    E = np.array([CO, NOx, NO, NO2, CO2, CxHy])
    
    EO2 = E * ((20.9 - 15)/(20.9 - O2))
    
    emiDict={'CO 15%-O2':round(EO2[0],2), 'NOx 15%-O2':round(EO2[1],2),
             'NO 15%-O2':round(EO2[2],2), 'NO2 15%-O2':round(EO2[3],2),
             'CO2 15%-O2':round(EO2[4],2), 'CxHy 15%-O2':round(EO2[5],2)}
    
    return (emiDict)

##--------------------------------------------[Function Testing]---------------
# data=[16.77,1584,6.5,6,0.5,2.21,4554]

# print(O2Basis(data[0],data[1],data[2],data[3],data[4],data[5],data[6]))

#------------------------------------------------------------------------------


