"""
*******************************************************************************
            RADIATION CORRECTION FOR TEMPERATURE (Traditional Method)
...............................................................................
DESCRIPTION:
Assumptions:
    1. Conduction from the bead to wire is neglected (Traditional Method)
    2. Overall equivalence ratio is used for exhaust product velocity and 
       property calculations using cantera framework
...............................................................................
REFERENCES:
    1.A correctional calculation method for thermocouple measurements of 
      temperatures in flames
    2.A New Method To Compute the Radiant Correction of Bare-Wire 
      Thermocouples
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
# import cantera as ct
import gasProperties as gp
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# pressure         [Pa]            
# temperature      [K]
# measured temperature (Tbead)  [K]
# equi_ratio       [-]
# overall equivalence ratio (ERO)  [-]
# mdot of tvc duct flow (mdot_tvc)  [g/s] -> [kg/s]
#------------------------------------------------------------------------------
    
def RadiationCorrection(Tbead, pressure, ERO, mdot_tvc, 
                        Tambient=300, d=1/1000, e=0.20, A_main=50*20*1e-6):
    """
    Radiation correction of temperature measurement.

    Parameters
    ----------
    Tbead : float
        Temperature of bead.
        Unit = [K]
    pressure : float
        Operating pressure of thermocouple environment.
        Unit = [Pa]
    ERO : float
        Overall equivalence ratio.
        Unit = [-]
    mdot_tvc : float
        Total flow rate through TVC.
        Unit = [kg/s]
    Tambient : float, optional
        Ambient temperature. The default is 300.
        Unit = [K]
    d : float, optional
        Diameter of thermocouple bead. The default is 1/1000.
        Unit = [m]
    e : float, optional
        Emissivity of thermocouple bead. The default is 0.20.
        Unit = [-]
    A_main : float, optional
        Area of the combustor duct. The default is 50*20*1e-6.
        Unit = [m^2]

    Returns
    -------
    T : float
        Corrected temperature of the fluid flow.
        Unit = [K]

    """
    print("\n>> Running Radiation Correction")
    sigma = 5.67e-8    # Stefans Constant (sigma)    [-]  
    T=Tbead
    
    for i in range(50):
        Tgas=T
        gasProperties=gp.MethaneAirEquilibrium(ERO, pressure, Tgas)
        surfaceProp=gp.MethaneAirEquilibrium(ERO, pressure, Tbead)
        V = mdot_tvc/(gasProperties[0]*A_main)
        
        nu, Re, Pr = gp.NuSphere(V,d,gasProperties[0],gasProperties[1],
              gasProperties[2],gasProperties[3],surfaceProp[1])
        
        T = Tbead+((e*sigma*d)/(gasProperties[3]*nu))*(Tbead**4-Tambient**4)
        
        if round(Tgas,0) == round(T,0):
            print("Velocity =",round(V,2),"[m/s]; ", "Re=",round(Re,2), "[-]")
            print("Tbead =",Tbead,"[K]; ", "Tcorrected = ",round(T,1),"[K]; ",
                  ", correction = ",round(T-Tbead,2),"[K]")
            break
        
    return T

##--------------------------------------------[Function Testing]---------------

# A = RadiationCorrection(1500, 101325*1, 0.5, 60/1000)

#------------------------------------------------------------------------------


