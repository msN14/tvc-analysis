"""
*******************************************************************************
                SYSTEMATIC ERRORS OF MEASUREMENT DEVICES
...............................................................................
DESCRIPTION:
    1) Input is measured values and id of the device
    2) Output is fractional error (error/value)
...............................................................................
REFERENCES:
    1. Temperature : Class 2 Thermocouple Accuracy Chart from Heatcon
    2. Pressure : GE UNIK 5000 Series static pressure sensor data sheet
    2. Emission : Testo 350 emission analyzer instruction manual
...............................................................................
DEVELOPER: NISANTH M. S, msn.
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
import numpy as np
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# quantity         [unit]            
#------------------------------------------------------------------------------

#---------------------------------------------[Systematic Errors]--------------
def thermoErr(value, T):
    '''
    Thermocouple Accuracy from Manufacturer

    Parameters
    ----------
    value : TYPE
        DESCRIPTION.
    T : TYPE
        DESCRIPTION.

    Returns
    -------
    ferr : TYPE
        DESCRIPTION.

    '''
    # Accuracy of K type Thermocouple
    if T == "K": 
        
        err1 = 0.0075 * value
        err2 = 2.5
        
        if err1 > err2:
            err = err1
        else :
            err = err2
    
    # Accuracy of S type Thermocouple
    elif T == "S": 
        
        err1 = 0.0025 * value
        err2 = 1.5
        
        if err1 > err2:
            err = err1
        else :
            err = err2
            
    # Accuracy of B type Thermocouple
    elif T == "B": 
        
        err1 = 0.0025 * value
        err2 = 1.5
        
        if err1 > err2:
            err = err1
        else :
            err = err2
            
    ferr = err/value # relative error
                   
    return ferr

def pressureErr(value, P):
    '''
    Accuracy of Static Pressure Sensor

    Parameters
    ----------
    value : TYPE
        DESCRIPTION.
    P : TYPE
        DESCRIPTION.

    Returns
    -------
    ferr : TYPE
        DESCRIPTION.

    '''
    
    # Accuracy of P7 Static Pressure Sensor
    if P == "P7":
        err = (0.04/100)*40 # [bar]
            
    ferr = err/value # relative error
                   
    return ferr


def testoErr(value, Species):
    '''
    Accuracy of Testo 350 Emission Analyzer

    Parameters
    ----------
    value : TYPE
        DESCRIPTION.
    Species : TYPE
        DESCRIPTION.

    Returns
    -------
    ferr : TYPE
        DESCRIPTION.

    '''
    
    if Species == "O2":
        err = 0.2
        
    elif Species == "CO":
        if value <=199:
            err = 10 # +/- 10 ppm
        elif value >= 200 and value <= 2000:
            err = 0.05*value # +/- 5% of reading
        else :
            err = 0.1*value # +/- 10% of reading
            
    elif Species == "CO2":
        # using equation : CO2 = (CO2_max/O2_ref)x(O2_ref - O2)
        err = (11.90/3)*0.2
        
    elif Species == "NO":
        if value <=99:
            err = 5 # +/- 5 ppm
        elif value >= 100 and value <= 1999.9:
            err = 0.05*value # +/- 5% of reading
        else :
            err = 0
            
    elif Species == "NO2":
        if value <=99:
            err = 5 # +/- 5 ppm
        else :
            err = 0.05*value # +/- 5% of reading
            
    elif Species == "NOx":
        if value <=99:
            err = np.sqrt(5**2 + 5**2) # +/- 5 ppm
        else :
            err = np.sqrt((0.05*value)**2 + (0.05*value)**2) # +/- 5% of reading
                
    elif Species == "CxHy":
        if value >= 100 and value <= 4000:
            err = 400 # +/- 400 ppm
        else :
            err = 0.1*value
    
    # if value = 0, don't calcuate ferr
    if value != 0:
        ferr = err/value
    else :
        ferr = 0
        
    return ferr
#------------------------------------------------------------------------------

#---------------------------------------------[Pattern Factor Error Calculator]
def PFerror(Profile, errorReport):
    '''
    Error in Pattern Factor

    Parameters
    ----------
    Profile : TYPE
        DESCRIPTION.
    errorReport : TYPE
        DESCRIPTION.

    Returns
    -------
    s_PF : TYPE
        DESCRIPTION.

    '''
    max_key = max(Profile, key=Profile.get) # key of the maximum temperature
    # print("max key = ", max_key)
    
    # STD of the max temperature
    s_max = errorReport[max_key[0:7]+'Error'] 
    
    # STD of each temperature
    s_T5, s_T6, s_T7 = errorReport["T5 (K) Error"],\
                       errorReport["T6 (K) Error"],\
                       errorReport["T7 (K) Error"]
                       
    s_T1 = errorReport["T1 (K) Error"]
    
    # Mean Temperature
    Tmean = np.mean([Profile['T5 (K) corrected'], 
                     Profile['T6 (K) corrected'],
                     Profile['T7 (K) corrected']])
    
    # Numerator Mean and Error
    T_up = Profile[max_key] - Tmean
    s_up = np.sqrt(np.sum(np.square([s_max, s_T5, s_T6, s_T7])))
    # Denominator Mean and Error
    T_down = Tmean - 300
    s_down = np.sqrt(np.sum(np.square([s_T5, s_T6, s_T7, s_T1])))
    
    # STD of the Pattern Factor
    s_PF = (T_up/T_down)*np.sqrt(np.sum((s_up/T_up)**2 + (s_down/T_down)**2))
        
    return s_PF
#------------------------------------------------------------------------------

#---------------------------------------------[Function Testing]---------------

#------------------------------------------------------------------------------


