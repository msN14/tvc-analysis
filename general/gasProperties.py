"""
*******************************************************************************
                        GAS MIXTURE COMPOSITIONS
...............................................................................
DESCRIPTION:
    1. Calculating gas mixture properties using CANTERA
...............................................................................
REFERENCES:
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
import cantera as ct
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# pressure         [Pa]            
# temperature      [K]
# equi_ratio       [-]
#------------------------------------------------------------------------------


def MethaneAir(equi_ratio, pressure, temperature):
    """
    Properties of methane-air mixture.

    Parameters
    ----------
    equi_ratio : float
        Equivalence ratio of the mixture.
        Unit = [-]
    pressure : float
        Pressure of the mixture.
        Unit = [Pa]
    temperature : float
        Temperature of the mixture.
        Unit = [K]

    Returns
    -------
    list
        Density and mass fraction of mixture.
        List = [density, [YCH4, YO2, YN2]]

    """
    gas = ct.Solution('gri30.xml','gri30_mix')  
    if equi_ratio == 0: # No methane in the gas mixture
        gas.X = {'O2':2, 'N2':7.52}
    elif equi_ratio == 'np': # No air in the gas mixture
        gas.X = {'CH4':1}
    else:
        gas.X = {'CH4':1, 'O2':2/equi_ratio, 'N2':7.52/equi_ratio}
    gas.TP = temperature, pressure
    return [gas.density,[gas.Y[gas.species_index('CH4')],
                         gas.Y[gas.species_index('O2')],
                         gas.Y[gas.species_index('N2')]]]


def MethaneAirEquilibrium(equi_ratio, pressure, temperature):
    """
    Properties of methane-air equilibrium mixture.

    Parameters
    ----------
    equi_ratio : float
        Equivalence ratio of the mixture.
        Unit = [-]
    pressure : float
        Pressure of the mixture.
        Unit = [Pa]
    temperature : float
        Temperature of the mixture.
        Unit = [K]

    Returns
    -------
    list
        Density, viscosity, cp and k of mixture.
        List = [density, viscosity, Cp, thermal conductivity]

    """
    gas = ct.Solution('gri30.xml','gri30_mix')  
    if equi_ratio == 0: # No methane in the gas mixture
        gas.X = {'O2':2, 'N2':7.52}
    elif equi_ratio == 'np': # No air in the gas mixture
        gas.X = {'CH4':1}
    else:
        gas.X = {'CH4':1, 'O2':2/equi_ratio, 'N2':7.52/equi_ratio}
    gas.TP = temperature, pressure
    gas.equilibrate('TP')
    
    return [gas.density, gas.viscosity, gas.cp_mass, gas.thermal_conductivity]


def NuSphere(Velocity,d,density,viscosity,Cp,k,viscositySurface):
    """
    Nusselt number of sphere. (For thermocouple bead)

    Parameters
    ----------
    Velocity : float
        Flow velocity over sphere.
        Unit = [m/s]
    d : float
        Diameter of sphere.
        Unit = [m]
    density : float
        Density of fluid flow.
        Unit = [kg/m^3]
    viscosity : float
        Viscosity of fluid flow.
        Unit = [kg/(m.s)] or [Pa.s]
    Cp : float
        Heat capacity of fluid flow.
        Unit = [J/K]
    k : float
        Thermal conductivity of fluid flow.
        Unit = [W/m.K]
    viscositySurface : float
        Viscosity of sphere surface.
        Unit = [kg/(m.s)] or [Pa.s]

    Returns
    -------
    nu : float
        Nusselt's number of sphere.
    Re : float
        Reynold's number of flow.
    Pr : float
        Prandtl's number of flow.

    """
    Re=(density*Velocity*d)/viscosity
    Pr=(viscosity*Cp)/k
    
    if Re<=400: 
        #Clift Correlation : Defined for Re < 400
        nu=1+((1+Re*Pr)**(1/3))*(Re**0.077)
    else : 
        #Wittekker Coorelation : Bound to very large Re range
        nu=2.0+(0.4*(Re**0.5)+0.06*(Re**0.67))*(Pr**0.4)*\
        ((viscosity/viscositySurface)**0.25)
        
    return nu,Re,Pr

##--------------------------------------------[Function Testing]---------------

# A = MethaneAirEquilibrium(0.5, 101325, 2000)

#------------------------------------------------------------------------------


