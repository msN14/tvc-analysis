"""
*******************************************************************************
                EMISSION ANALYSIS (EI(i) AND COMBUSTION EFFICIENCY)
...............................................................................
DESCRIPTION:
    1) Function input = CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet in X(i)
       Mole fraction Entry (X(i))
       
    2) Output is Emission index dictionary

    CmHnOpNqSr + X[R(O2)+S(N2)+T(CO2)+h(H2O)+U(CH4)] -->
        P1(CO2+P2(N2)+P3(O2)+P4(H2O)+P5(CO)+P6(CxHy)+P7(NO2)+P8(NO)+P9(SO2))
...............................................................................
REFERENCES:
    1. Procedure for the Analysis and Evaluation of Gaseous Emissions 
       from Aircraft Engines (SAE ARP1533B-2016)
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##--------------------------------------------[Imports]------------------------
# import cantera as ct
import numpy as np
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# pressure         [Pa]            
# temperature      [K]
# equi_ratio       [-]
# CO_wet .....     [-]    (X values)
#------------------------------------------------------------------------------

def EI(CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet):
    """
    Calculation of Emission Index (EIs) and combustion efficiency.
    
    All necessary constant values are defined inside the function.
    
    Parameters
    ----------
    CO_wet : float
        Mole fraction of CO in wet measurement.
    NOx_wet : float
        Mole fraction of NOx in wet measurement.
    NO_wet : float
        Mole fraction of NO in wet measurement.
    CO2_wet : float
        Mole fraction of CO2 in wet measurement.
    CxHy_wet : float
        Mole fraction of CxHy in wet measurement.

    Returns
    -------
    emiDict : dict
        Emission index in dictionary format.
        [EICO, EINO, EINOx, EICxHy, Efficiency]

    """     
    # Molecular weights of species
    M = {'AIR':28.965,'C':12.011,'H':1.008,'N':14.0067,'O':15.9994,'CO':28.01,
         'CH4':16.04,'NO':30.01,'NO2':46.0055}
    
    m,n,p,q,r = 1,4,0,0,0 # Molar Constant for Fuel CmHnOpNqSr
    
    # Mole fraction of O2,N2+Ar,CO2,CH4,water vapor per mole of dry inlet air
    R,S,T,U,h = 0.20948,0.79020,0.00034,0,0 
    
    x,y = 1,4 # Molar Constants selected for UHC 
    Hc = 50e6 # Lower heating value of the fuel (LHV)  
                           
    A = np.array([[0,1,0,0,0,1,x,0,0,0,-T-U],
                  [0,0,0,0,2,0,y,0,0,0,-2*h-4*U],
                  [0,2,0,2,1,1,0,2,1,2,-2*R-2*T-h],
                  [0,0,2,0,0,0,0,1,1,0,-2*S],
                  [0,0,0,0,0,0,0,0,0,1,0],
                  [CO2_wet,-1,0,0,0,0,0,0,0,0,0],
                  [CO_wet,0,0,0,0,-1,0,0,0,0,0],
                  [CxHy_wet,0,0,0,0,0,-x,0,0,0,0],
                  [NOx_wet,0,0,0,0,0,0,-1,-1,0,0],
                  [NO_wet,0,0,0,0,0,0,0,-1,0,0],
                  [-1,1,1,1,1,1,1,1,1,1,0]])
    
    C=np.array([m,n,p,q,r,0,0,0,0,0,0]) 
    B = np.linalg.solve(A, C) #[PT,P1,P2,P3,P4,P5,P6,P7,P8,P9,X]
    EICO=(B[5]*M['CO']*1e3)/(m*M['C']+n*M['H'])
    EINO=(B[8]*M['NO']*1e3)/(m*M['C']+n*M['H'])
    EINOx=((B[7]+B[8])*M['NO2']*1e3)/(m*M['C']+n*M['H'])
    EICxHy=(B[6]*M['CH4']*1e3)/(m*M['C']+n*M['H'])
    
    # equation for combustion efficiency calculation
    eff=(1-(10109*(EICO/Hc))-(EICxHy/1000))*100
    # formatting EI(i) values
    emiDict={'EICO':round(EICO,2),'EINO':round(EINO,2),'EINOx':round(EINOx,2),\
             'EICxHy':round(EICxHy,2),'Efficiency':round(eff,2)}
        
    return emiDict

##--------------------------------------------[Function Testing]---------------
# [% O2	ppm CO	ppm NOx	ppm NO	ppm NO2	% CO2	ppm CxHy]

# data = [16.77, 1584, 6.5, 6, 0.5, 2.21, 4554]

# D = [13.49, 1500, 5.98, 5.92, 0.05, 3.94, 361.72, 1400]
# CO, NOx, NO, CO2, CxHy  = D[1]*1e-6, D[2]*1e-6, D[3]*1e-6, D[5]*1e-2, D[7]*1e-6

# result = EI(CO, NOx, NO, CO2, CxHy)

# print(result)
#------------------------------------------------------------------------------

def EIall(CO_wet, NOx_wet, NO_wet, CO2_wet, CxHy_wet, O2_wet, Error):
    """
    Calculation of Emission Index (EIs) and combustion efficiency.
    
    All necessary constant values are defined inside the function.
    
    Parameters
    ----------
    CO_wet : float
        Mole fraction of CO in wet measurement.
    NOx_wet : float
        Mole fraction of NOx in wet measurement.
    NO_wet : float
        Mole fraction of NO in wet measurement.
    CO2_wet : float
        Mole fraction of CO2 in wet measurement.
    CxHy_wet : float
        Mole fraction of CxHy in wet measurement.
    O2_wet : float
        Mole fraction of O2 in wet measurement.

    Returns
    -------
    emiDict : dict
        Emission index in dictionary format.
        [EICO, EINO, EINOx, EICxHy, Efficiency]

    """     
    # Molecular weights of species
    M = {'AIR':28.965,'C':12.011,'H':1.008,'N':14.0067,'O':15.9994,'CO':28.01,
         'CH4':16.04,'NO':30.01,'NO2':46.0055}
    
    m,n,p,q,r = 1,4,0,0,0 # Molar Constant for Fuel CmHnOpNqSr
    
    # Mole fraction of O2,N2+Ar,CO2,CH4,water vapor per mole of dry inlet air
    R,S,T,U,h = 0.20948,0.79020,0.00034,0,0 
    
    x,y = 1,4 # Molar Constants selected for UHC 
    Hc = 50e6 # [J/Kg] # Lower heating value of the fuel (LHV) 
    Hc1 = 21496 # [Btu/lb]
                           
    A = np.array([[0,1,0,0,0,1,x,0,0,0,-T-U],
                  [0,0,0,0,2,0,y,0,0,0,-2*h-4*U],
                  [0,2,0,2,1,1,0,2,1,2,-2*R-2*T-h],
                  [0,0,2,0,0,0,0,1,1,0,-2*S],
                  [0,0,0,0,0,0,0,0,0,1,0],
                  [CO2_wet,-1,0,0,0,0,0,0,0,0,0],
                  [CO_wet,0,0,0,0,-1,0,0,0,0,0],
                  [CxHy_wet,0,0,0,0,0,-x,0,0,0,0],
                  [NOx_wet,0,0,0,0,0,0,-1,-1,0,0],
                  [NO_wet,0,0,0,0,0,0,0,-1,0,0],
                  [-1,1,1,1,1,1,1,1,1,1,0]])
    
    C=np.array([m,n,p,q,r,0,0,0,0,0,0]) 
    B = np.linalg.solve(A, C) #[PT,P1,P2,P3,P4,P5,P6,P7,P8,P9,X]
    EICO=(B[5]*M['CO']*1e3)/(m*M['C']+n*M['H'])
    EINO=(B[8]*M['NO']*1e3)/(m*M['C']+n*M['H'])
    EINOx=((B[7]+B[8])*M['NO2']*1e3)/(m*M['C']+n*M['H'])
    EICxHy=(B[6]*M['CH4']*1e3)/(m*M['C']+n*M['H'])
    
    # equation for combustion efficiency calculation
    eff=(1-(10109*(EICO/Hc))-(EICxHy/1000))*100
    # formatting EI(i) values

        
    #--------------------------------------------------------------------------
    # Calculation of Uncertainty
    SUM = CO_wet + CO2_wet + CxHy_wet
    alpha = n/m
    X = m * ((1-((alpha/4)*SUM))/(((1+h)*SUM)-1))
    
    EICO1 = (CO_wet/SUM)*(1e3)*(M['CO']/(M['C']+alpha*M['H']))*(1+((T*X)/m))
    EINOx1 = (NOx_wet/SUM)*(1e3)*(M['NO2']/(M['C']+alpha*M['H']))*(1+((T*X)/m))
    EICxHy1 = (CxHy_wet/SUM)*(1e3)*(M['CH4']/(M['C']+alpha*M['H']))*(1+((T*X)/m))
    # eff1 = (1-(10109*(EICO1/Hc))-(EICxHy1/1000))*100
    eff1 = (1-(4.346*(EICO1/Hc1))-(EICxHy1/1000))*100
    
    #--------------------------------------------------------------------------
    # Instrument Errors
    # dCO, dCO2, dCxHy, dNOx = 0.05*CO_wet, 0.003+0.01*CO2_wet, 400e-6, 7e-6
    
    ers = Error
    
    dCO, dNOx, dCO2, dCxHy = ers['ppm CO Error']*1e-6, ers['ppm NOx Error']*1e-6,\
                             ers['% CO2 Error']*1e-2,  ers['ppm CxHy Error']*1e-6
        
    #--------------------------------------------------------------------------
    RHL1 = (T/(m+T*X))*(((-m)*(1+h-T*(alpha/4)))/((((1+h)*SUM)-T)**2))
    #--------------------------------------------------------------------------
    EICO_CO = ((CO2_wet+CxHy_wet)/(SUM)) + RHL1 * CO_wet
    EICO_CO2 = ((-CO2_wet)/(SUM)) + RHL1 * CO2_wet
    EICO_CxHy = ((-CxHy_wet)/(SUM)) + RHL1 * CxHy_wet
    rEICO = np.sqrt((EICO_CO**2)*((dCO/CO_wet)**2) +
                    (EICO_CO2**2)*((dCO2/CO2_wet)**2) +
                    (EICO_CxHy**2)*((dCxHy/CxHy_wet)**2))
    #--------------------------------------------------------------------------
    EINOx_CO = ((-CO_wet)/(SUM)) + RHL1 * CO_wet
    EINOx_CO2 = ((-CO2_wet)/(SUM)) + RHL1 * CO2_wet
    EINOx_CxHy = ((-CxHy_wet)/(SUM)) + RHL1 * CxHy_wet
    EINOx_NOx = 1
    rEINOx = np.sqrt((EINOx_CO**2)*((dCO/CO_wet)**2) +
                    (EINOx_CO2**2)*((dCO2/CO2_wet)**2) +
                    (EINOx_CxHy**2)*((dCxHy/CxHy_wet)**2)+
                    (EINOx_NOx**2)*((dNOx/NOx_wet)**2))
    #--------------------------------------------------------------------------
    EICxHy_CO = ((-CO_wet)/(SUM)) + RHL1 * CO_wet
    EICxHy_CO2 = ((-CO2_wet)/(SUM)) + RHL1 * CO2_wet
    EICxHy_CxHy = ((CO_wet+CO2_wet)/(SUM)) + RHL1 * CxHy_wet
    rEICxHy = np.sqrt((EICxHy_CO**2)*((dCO/CO_wet)**2) +
                    (EICxHy_CO2**2)*((dCO2/CO2_wet)**2) +
                    (EICxHy_CxHy**2)*((dCxHy/CxHy_wet)**2))
    #--------------------------------------------------------------------------
    eff_CO = (-EICO1)/(((100*Hc1)/4.346)-EICO1-((Hc1/4346)*EICxHy1))
    eff_CxHy = (-EICxHy1)/(1000 - ((4346/Hc1)*EICO1) - EICxHy1)
    rEff = np.sqrt((eff_CO**2)*(rEICO**2) + (eff_CxHy**2)*(rEICxHy**2))
    #--------------------------------------------------------------------------
    emiDict = {'EICO':round(EICO,2),'EINO':round(EINO,2),'EINOx':round(EINOx,2),
             'EICxHy':round(EICxHy,2),'Efficiency':round(eff,2),
             'EICO new':round(EICO1,2),'EINOx new':round(EINOx1,2),
             'EICxHy new':round(EICxHy1,2),'Efficiency new':round(eff1,2)}
    
    emiDictError = {'EICO Error':round(round(rEICO,2)*round(EICO,2),2), 
                    'EINOx Error':round(round(rEINOx,2)*round(EINOx,2),2),
                    'EICxHy Error':round(round(rEICxHy,2)*round(EICxHy,2),2),
                    'Efficiency Error':round(round(rEff,3)*round(eff,2),2),
                    'EICO new Error':round(round(rEICO,2)*round(EICO1,2),2), 
                    'EINOx new Error':round(round(rEINOx,2)*round(EINOx1,2),2),
                    'EICxHy new Error':round(round(rEICxHy,2)*round(EICxHy1,2),2),
                    'Efficiency new Error':round(round(rEff,3)*round(eff1,2),2)}
    
    return emiDict, emiDictError

##--------------------------------------------[Function Testing]---------------
# [% O2	ppm CO	ppm NOx	ppm NO	ppm NO2	% CO2	ppm CxHy]

# data = [16.77, 1584, 6.5, 6, 0.5, 2.21, 4554]

# D = [13.49, 1500, 5.98, 5.92, 0.05, 3.94, 361.72, 1400]
# D = [13.41,	656.3, 5.94, 4.43, 1.51, 4.25, 278.08, 815.17]
# D = [13.41,	656.3, 5.94, 4.43, 1.51, 4.25, 278.08, 815.17]

# CO, NOx, NO, CO2, CxHy, O2  = D[1]*1e-6, D[2]*1e-6, D[3]*1e-6, D[5]*1e-2, D[7]*1e-6, D[0]*1e-2

# result = EIall(CO, NOx, NO, CO2, CxHy, O2, Err)

# print(result)
#------------------------------------------------------------------------------



