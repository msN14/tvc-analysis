#---------------------------------------------[Imports]------------------------
import tvcObject as to
#------------------------------------------------------------------------------

#---------------------------------------------[Conditions]---------------------

C = to.form1(101325, 300, 10, 5, 1.75, 0, 0)
# C = to.form2()
# C = to.form3()

area_ratio = 1/10 # 1/10 for slit geometry
# area_ratio = 16.68/50 # slit geometry of 3 struts 16.68mm

print('\n===============================================================')
print('\nMain Flow Parameters ::')
print('\nmdot_main_3D = ', round(C.mdot_main*1000,4), '[g/s]')
print('\nmdot_main_2D = ', round(C.mdot_main*1000*(area_ratio),4), '[g/s]')
print('\nY_main => [YCH4, YO2, YN2] = ',
      [round(C.YCH4_main,4),round(C.YO2_main,4),round(C.YN2_main,4)])
print('Parity Check = ', 
      round(C.YCH4_main,4)+round(C.YO2_main,4)+round(C.YN2_main,4))
if (round(C.YCH4_main,4)+round(C.YO2_main,4)+round(C.YN2_main,4)) != 1:
    print('\n',round(C.YCH4_main,5), round(C.YO2_main,5), round(C.YN2_main,5))
print('\n---------------------------------------------------------------')
print('\nCavity Flow Parameters ::')
print('\nUx = ', round(C.Ux,2), '[m/s]')
print('\nmdot_cavity_3D = ', round(C.mdot_cavity*1000,4), '[g/s]')
print('\nmdot_cavity_2D = ', round(C.mdot_cavity*1000*(area_ratio),4), '[g/s]')
print('\nY_cavity => [YCH4, YO2, YN2] = ',
      [round(C.YCH4_cavity,4),round(C.YO2_cavity,4),round(C.YN2_cavity,4)])
print('Parity Check = ', 
      round(C.YCH4_cavity,4)+round(C.YO2_cavity,4)+round(C.YN2_cavity,4))

print('\n---------------------------------------------------------------')
print('\nGlobal Parameters ::')
print('\nERO = ', round(C.ERO,2), '[-]', C.ERO)
print('\nCavity Power = ', round(C.power_cavity,2), '[kW]')
print('Main Power   = ', round(C.power_main,2), '[kW]')
print('Total Power  = ', round(C.power,2), '[kW]')
print('\n===============================================================')
#------------------------------------------------------------------------------
