
import xlrd as xl
xl.xlsx.ensure_elementtree_imported(False, None)
xl.xlsx.Element_has_iter = True

def reportDict(resultFile,sheet=0): # sheet number starting from 0
    loc = (resultFile) #Name of the CASE and FIle
    wb = xl.open_workbook(loc) #Opening Workbook
    sheet = wb.sheet_by_index(sheet) #Opening Specific Excel workseet
    dataDict = {} #Return Variable colum[0]=heading and colum[1:]=data
    for i in range(sheet.ncols):
        ElementColumn=[]
        for j in range(sheet.nrows):
            Element=sheet.cell_value(j, i)
            ElementColumn.append(Element)
        dataDict[ElementColumn[0]] = ElementColumn[1:]
    return dataDict