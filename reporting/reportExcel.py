import xlsxwriter as xw

def caseVariable(caseName):
    #Reading the CaseName
    c=caseName
    caseDict={}
    caseDict['P']=float(c[c.find('P')+1:c.find('T')])
    caseDict['T']=float(c[c.find('T')+1:c.find('LD')])
    caseDict['LD']=float(c[c.find('D')+1:c.find('t')])
    caseDict['t']=float(c[c.find('t')+1:c.find('V')])
    caseDict['V']=float(c[c.find('V')+1:c.find('J')])
    caseDict['J']=float(c[c.find('J')+1:c.find('ERC')])
    caseDict['ERC']=float(c[c.find('C')+1:c.find('ERM')])
    caseDict['ERM']=float(c[c.find('M')+1:c.find('DA')])
    caseDict['DA']=float(c[c.find('A')+1:])
    #Returning the Case Variables in Dictionary Format for writing to Excel
    return(caseDict)

def reportExcel(ID, conditionDataReport, mfcDataReport,
                tempDataReport, tempCorrReport, pressureDataReport,
                emissionDataReport, emissionO2Results, emissionResults):
    # print("\n>> Creating Excel Report ..............")
    
    variable=caseVariable(ID)
    
    workbook = xw.Workbook(ID+'/'+ID+'.xlsx')
    worksheet = workbook.add_worksheet()
    
    cell_format = workbook.add_format()
    cell_format.set_align('center')
    cell_format.set_align('vcenter')
    cell_format.set_text_wrap()
    
    #Writing Conditions Data
    col=1
    for key, value in variable.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=11   
    for key, value in conditionDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=27
    for key, value in mfcDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=40
    for key, value in tempDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=50
    for key, value in tempCorrReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=60
    for key, value in pressureDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=68
    for key, value in emissionDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=77
    for key, value in emissionO2Results.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=84
    for key, value in emissionResults.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    workbook.close()
    
def reportExcelAll(ID, conditionDataReport, mfcDataReport,
                   tempDataReport, tempCorrReport, tempErrReport,
                   pressureDataReport,pressureErrReport,
                   emissionDataReport, emissionErrReport,
                   O2Results, O2Error,
                   eiResults, eiError, suffix):
    # print("\n>> Creating Excel Report ..............")
    
    variable=caseVariable(ID)
    
    workbook = xw.Workbook(ID+'/'+ID+'-'+suffix+'.xlsx')
    worksheet = workbook.add_worksheet()
    
    cell_format = workbook.add_format()
    cell_format.set_align('center')
    cell_format.set_align('vcenter')
    cell_format.set_text_wrap()
    worksheet.set_row(0, 50)
    
    #Writing Conditions Data
    col=1
    for key, value in variable.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=col+1   
    for key, value in conditionDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=col+1
    for key, value in mfcDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in tempDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=col+1
    for key, value in tempCorrReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in tempErrReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    col=col+1
    for key, value in pressureDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=80 # fixed 
    for key, value in pressureErrReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in emissionDataReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in emissionErrReport.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in O2Results.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in O2Error.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in eiResults.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
    
    col=col+1
    for key, value in eiError.items():
        worksheet.write(0, col, key, cell_format)
        worksheet.write(1, col, value, cell_format)
        col += 1
    #    print(col)
        
    workbook.close()
