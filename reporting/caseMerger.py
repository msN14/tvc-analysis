# Merging Multiple Excel Workbooks

import xlrd as xl
import xlsxwriter as xw


def dataExtract(caseName):
    loc = (caseName+'.xlsx')  
    wb = xl.open_workbook(loc) 
    sheet = wb.sheet_by_index(0)
    Heading=[]  
    Elements = []
    caseData=[]
    
    for i in range(sheet.ncols):
        cHeading=sheet.cell_value(0, i)
        cElement=sheet.cell_value(1, i)
        Heading.append(cHeading)
        Elements.append(cElement)
    mergeHeading=Heading
    mergeElements=Elements
    caseData.append(mergeHeading)
    caseData.append(mergeElements)
    return(caseData)
        
ERC15ERM0=dataExtract("P1T300LD1.0t2V10J10.0ERC1.5ERM0.0DA1.0")
ERC15ERM02=dataExtract("P1T300LD1.0t2V10J10.0ERC1.5ERM0.2DA1.0")
ERC15ERM03=dataExtract("P1T300LD1.0t2V10J10.0ERC1.5ERM0.3DA1.0")
ERC15ERM04=dataExtract("P1T300LD1.0t2V10J10.0ERC1.5ERM0.4DA1.0")

ERC20ERM0=dataExtract("P1T300LD1.0t2V10J10.0ERC2.0ERM0.0DA1.0")
ERC20ERM02=dataExtract("P1T300LD1.0t2V10J10.0ERC2.0ERM0.2DA1.0")
ERC20ERM03=dataExtract("P1T300LD1.0t2V10J10.0ERC2.0ERM0.3DA1.0")
ERC20ERM04=dataExtract("P1T300LD1.0t2V10J10.0ERC2.0ERM0.4DA1.0")

ERC30ERM02=dataExtract("P1T300LD1.0t2V10J10.0ERC3.0ERM0.2DA1.0")


workbook = xw.Workbook('Summary.xlsx')
worksheet = workbook.add_worksheet()

cell_format = workbook.add_format()
cell_format.set_align('center')
cell_format.set_align('vcenter')
cell_format.set_text_wrap()

#Writing Conditions Data
 
def rowEntry(row,rowNumber):
    col=1
    for item in (row):
        worksheet.write(rowNumber, col, item, cell_format)
        col += 1
        
rowEntry(ERC15ERM0[0][0:],0)
rowEntry(ERC15ERM0[1][0:],1)
rowEntry(ERC15ERM02[1][0:],2)
rowEntry(ERC15ERM03[1][0:],3)
rowEntry(ERC15ERM04[1][0:],4)

rowEntry(ERC20ERM0[1][0:],6)
rowEntry(ERC20ERM02[1][0:],7)
rowEntry(ERC20ERM03[1][0:],8)
rowEntry(ERC20ERM04[1][0:],9)

rowEntry(ERC30ERM02[1][0:],11)

    
workbook.close()